# CannonGame README #

### Understanding the flow of the code ###
The core of the code is stored in the core project folder. It contains everything used in the desktop, android, ios, and html games. When a launcher is run from the other 4 code sources the CannonGame class is called and begins to render the game.

#### The majority of the work that needs to be done is in GameManager.java. This file is essentially the core of the game. ####

### Setting up your environment ###
This application will run compile successfully on a properly set up eclipse environment.

1. Download the latest Eclipse IDE from the eclipse website.
    https://eclipse.org/downloads/
2. Extract and Run
3. Follow these instructions to get the android environment setup

    Start Eclipse, then select Help > Install New Software.
    Click Add, in the top-right corner.
    In the Add Repository dialog that appears, enter "ADT Plugin" for the Name and the following URL for the Location:

    https://dl-ssl.google.com/android/eclipse/

    Note: The Android Developer Tools update site requires a secure connection. Make sure the update site URL you enter starts with HTTPS.
    Click OK.
    In the Available Software dialog, select the checkbox next to Developer Tools and click Next.
    In the next window, you'll see a list of the tools to be downloaded. Click Next.
    Read and accept the license agreements, then click Finish.

    If you get a security warning saying that the authenticity or validity of the software can't be established, click OK.
    When the installation completes, restart Eclipse.

4. Install Gradle plugin (Resource management)
    Using the same method to add software to eclipse add the following repo and install gradle from: http://dist.springsource.com/release/TOOLS/gradle

5a. Copy https://bitbucket.org/tjb1991/cannongamejonathan.git to your clipboard
5b. In Eclipse, go to File -> Import -> Git (dropdown) -> Projects from Git -> Clone URI -> The information should already be filled out, just put your username and password -> Next -> All projects should be checked -> Finish

6. Wait for the repository to be pulled down to your environment

7. Next, you'll need to convert your project to a gradle project, so you don't have to worry about the dependencies. Select all of the Cannon game project folders on the left and right click -> Configure -> Convert to Gradle Project
Then, once that has completed you'll need to pull down dependencies. Right click again on all selected projects -> Gradle -> Refresh All. Wait for this to complete. You should be ready to code now if no errors exist in your project folders.

### Code Structure ###

The cannon game is written to be as atomic as possible where components can be changed without affecting other areas too heavily. Additionally, it's based on the LibGDX programming library with many built in functions to make coding easier.

To follow the control structure you'll have to understand what each folder in your project hierarchy is for.

#### Folders ####
1. /Cannon-android    -> Contains all android specific runtime code
  1a. /assets         -> If you want to add textures, sounds or music put it here
  1b. /src
    1b1. com.antorian.cannon.android             -> Run this if you have your Android hooked up with debugging enabled and driver installed
2. /Cannon-core       -> Contains the actual game code
  2a. /src
    2a1. com.antorian.cannon.CannonGame.java     -> Starting point
3. /Cannon-desktop    -> Contains all PC specific runtime code
  3a. /src
    3a1. /com.antorian.cannon.desktop.DesktopLauncher.java    -> Run this to play on PC

#### Control Structure ####
The is a call stack of how the code is run.

Understand the Android App Lifecycle:

CREATE -----> RENDER -----> DESTROY
              |   ^
       --------   --------
      v                  ^
    PAUSE -----------> RESUME

1. DesktopLauncher.java/AndroidLauncher.java is Run
2. CannonGame.java
  a. create() is run by LibGDX on startup, it's only run once
  b. dispose() is run when the app closed, when you're destroying everything and don't need it anymore
  c. render() is called 30 or 60 times a second, this will impact your framerate
  d. resize() is called when you resize your window in windows or rotate the screen in android, rotation is disabled though.
  e. pause() is called when the app goes into the background
  f. resume() is called when the app returns from the background
Notes: All revolves around your current screen, for this app, there is only 3, the code is generalized for many.
3. CannonGame.create() and render() -> new AntorianScreen()
  a. AntorianScreen.java
    a1. Displays the Antorian Logo
    a2. Sets up the next screen (Loading Screen)
  b. LoadingScreen.java
    b1. Creates an AssetManager to be passed around holding all assets loaded from the assets folder
    b2. Once the assets are loaded PlayScreen is set to the current screen
  c. PlayScreen.java
    c1. This class is the main playing screen and handles the GameManager/HUD/Physics
    c2. GameManager.java -> This is a very important class. Study this one primarily. It handles the World Objects, Penguin (Ball.java), CannonManager and PowerupManager (Among Other things)

### What is this repository for? ###

* Quick summary
* 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)