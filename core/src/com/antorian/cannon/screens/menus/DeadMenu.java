package com.antorian.cannon.screens.menus;

import com.antorian.cannon.CannonGame;
import com.antorian.cannon.GameState;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.support.database.DatabaseDAO;
import com.antorian.cannon.support.database.GameLog;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class DeadMenu extends Stage{
	
	CannonGameInterface game;
	
	private TextButton button1;
	private int finalScore;
	private int currentBest;
	
	private int currentCoins;
	private int totalCoins;
	
	public DeadMenu(CannonGameInterface game){
		this.game = game;
		
		Table table = new Table();
        table.setFillParent(true);
        this.addActor(table);
        

        TextButtonStyle style = new TextButtonStyle();
        BitmapFont font = new BitmapFont();
        style.font = font;
        //font.scale(2);
        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/ARIBLK.TTF"));
        FreeTypeFontParameter param = new FreeTypeFontParameter();
        param.size = 30;
        font = gen.generateFont(param);
        gen.dispose();
        style.font = font;
       
        
        button1 = new TextButton("", style);
        table.add(button1);
        table.row();

        TextButton button2 = new TextButton("Try Again", style);
        table.add(button2);
        table.row();
        
        TextButton button3 = new TextButton("Main Menu", style);
        table.add(button3);
        table.row();
        
        
        button2.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		restartGame();
				return true;
			}
        });
		
        button3.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		returnMainMenu();
				return true;
			}
        });
	}
	
	private void restartGame(){
		this.addAction(Actions.sequence(Actions.fadeOut(game.getHud().getMenuFadeTime()), Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.INIT_RESTART_FADE_OUT);
			}
		})));
	}
	
	private void returnMainMenu(){
		this.addAction(Actions.sequence(Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.INIT_RETURNMAIN_FADE_OUT);
			}
		})));
	}

	public void setFinalScore(int score) {
		finalScore = score;
		
		if(Gdx.app.getType() == ApplicationType.Android){
			currentBest = game.getDatabase().getTopScore();
		}
		if(score > currentBest && !Settings.cheat){
			currentBest = score;
		}
		game.getDatabase().storeScore(new GameLog(score));//Are we storing too much?
		updateButton1();
	}
	
	public void setFinalCoins(int coins){
		currentCoins = coins;
		totalCoins += coins;
		updateButton1();
	}
	
	private void updateButton1(){
		

		button1.setText("Final Score: " + finalScore + "\nAll Time Best: " + currentBest + "\nCurrent Coins: " + currentCoins + "\nTotal Coins: " + totalCoins +"\n\n");
	}
	
}
