package com.antorian.cannon.screens.menus;

import com.antorian.cannon.GameState;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class DebugMenu extends Stage{

	CannonGameInterface game;
	Table table;
	
	public DebugMenu(final CannonGameInterface game){
		this.game = game;
		table = new Table();
		
        table.setFillParent(true);
        this.addActor(table);
        

        TextButtonStyle style = new TextButtonStyle();
//        BitmapFont font = new BitmapFont();
//        style.font = font;
        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/ARIBLK.TTF"));
        FreeTypeFontParameter param = new FreeTypeFontParameter();
        param.size = 30;
        BitmapFont font = gen.generateFont(param);
        gen.dispose();
        style.font = font;
       
        
        final TextButton button1 = new TextButton("Menus debug: " + Settings.menuBoxDisplayDebug, style);
        table.add(button1);
        table.row();

        button1.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		Settings.menuBoxDisplayDebug = !Settings.menuBoxDisplayDebug;
        		button1.setText("Menus debug: " + Settings.menuBoxDisplayDebug);
				return true;
			}
        });
        
        final TextButton button2 = new TextButton("Physics debug: " + Settings.boxPhysicsDebug, style);
        table.add(button2);
        table.row();

        button2.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		Settings.boxPhysicsDebug = !Settings.boxPhysicsDebug;
        		button2.setText("Physics debug: " + Settings.boxPhysicsDebug);
				return true;
			}
        });
        
        final TextButton button3 = new TextButton("Cheat Mode Enabled: " + Settings.cheat, style);
        table.add(button3);
        table.row();

        button3.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		Settings.cheat = !Settings.cheat;
        		button3.setText("Cheat Mode Enabled: " + Settings.cheat);
				return true;
			}
        });
        
        final TextButton button4 = new TextButton("Coins Enabled: " + Settings.coinsEnabled, style);
        table.add(button4);
        table.row();

        button4.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		Settings.coinsEnabled = !Settings.coinsEnabled;
        		button4.setText("Coins Enabled: " + Settings.coinsEnabled);
				return true;
			}
        });
        
        final TextButton button5 = new TextButton("Trampolines Enabled: " + Settings.trampolineEnabled, style);
        table.add(button5);
        table.row();

        button5.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		Settings.trampolineEnabled = !Settings.trampolineEnabled;
        		button5.setText("Trampolines Enabled: " + Settings.trampolineEnabled);
				return true;
			}
        });
        
        final TextButton button6 = new TextButton("Packages Enabled: " + Settings.packagesEnabled, style);
        table.add(button6);
        table.row();
        table.row();

        button6.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		Settings.packagesEnabled = !Settings.packagesEnabled;
        		button6.setText("Packages Enabled: " + Settings.packagesEnabled);
				return true;
			}
        });
        
        final TextButton button7 = new TextButton("Clear Database", style);
        table.add(button7);
        table.row();
        table.row();

        button7.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		game.getDatabase().deleteDatabase();
        		button7.setText("Cleared");
				return true;
			}
        });
        
        TextButton button99 = new TextButton("Return to Main Menu", style);
        table.add(button99);
        table.row();

        button99.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		button7.setText("Clear Database");
        		game.setupWorld();
        		openMainMenu();
				return true;
			}
        });
		
	}
	
	private void openMainMenu(){
		this.addAction(Actions.sequence(Actions.fadeOut(game.getHud().getMenuFadeTime()), Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.INIT_MAINMENU);
			}
		})));
	}
	
	public Table getTable(){
		return table;
	}
}
