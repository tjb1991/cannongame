package com.antorian.cannon.screens.menus;

import com.antorian.cannon.GameState;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class HudMenu extends Stage{
	
	CannonGameInterface g;
	
	TextButton button1;
	boolean pauseEnabled = true;
	
	public HudMenu(CannonGameInterface game){
		this.g = game;
		
		Table table = new Table();
        table.setFillParent(true);
        this.addActor(table);
        

        TextButtonStyle style = new TextButtonStyle();
        BitmapFont font = new BitmapFont();
        //font.scale(2);
        style.font = font;
        
        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/ARIBLK.TTF"));
        FreeTypeFontParameter param = new FreeTypeFontParameter();
        param.size = 30;
        font = gen.generateFont(param);
        gen.dispose();
        style.font = font;
        
       
        
        button1 = new TextButton("Score", style);
        //button1.setTouchable(Touchable.disabled);
        table.add(button1);
        table.row();
        
        button1.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		if(pauseEnabled)
        			g.setGameState(GameState.INIT_PAUSE);
				return true;
			}
        });
	}
	
	public void updateScore(int score){
		if(Settings.autoFire > 0)
			button1.setText("AUTO FIRE " + Settings.autoFire + "\n" + (score-1));
		else
			button1.setText("" + score);
	}

	public void disablePause() {
		pauseEnabled = false;
	}
	
	public void enablePause(){
		pauseEnabled = true;
	}

}
