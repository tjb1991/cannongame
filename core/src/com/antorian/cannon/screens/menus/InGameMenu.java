package com.antorian.cannon.screens.menus;

import com.antorian.cannon.GameState;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class InGameMenu extends Stage{
	
	CannonGameInterface game;
	
	public InGameMenu(CannonGameInterface game){
		this.game = game;
		
		Table table = new Table();
        table.setFillParent(true);
        this.addActor(table);
        

        TextButtonStyle style = new TextButtonStyle();
        BitmapFont font = new BitmapFont();
        //font.scale(2);
        style.font = font;
        
       
        
        TextButton button1 = new TextButton("Resume", style);
        table.add(button1);
        table.row();

        button1.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		resumeGame();
				return true;
			}
        });
	}
	
	private void resumeGame(){
		this.addAction(Actions.sequence(Actions.fadeOut(game.getHud().getMenuFadeTime()), Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.PLAYING_RESUME);
			}
		})));
	}
	
	
	public void moveTo(){
		//http://www.gamefromscratch.com/post/2013/12/09/LibGDX-Tutorial-9-Scene2D-Part-2-Actions.aspx
//		MoveToAction moveAction = new MoveToAction();
//        moveAction.setPosition(300f, 0f);
//        moveAction.setDuration(10f);
//        myActor.addAction(moveAction);
//        
//        stage.addActor(myActor);
	}

}
