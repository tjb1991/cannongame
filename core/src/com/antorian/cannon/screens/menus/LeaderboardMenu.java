package com.antorian.cannon.screens.menus;

import java.util.ArrayList;

import com.antorian.cannon.GameState;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.support.database.GameLog;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class LeaderboardMenu extends Stage{

	CannonGameInterface game;
	int maxNumScores = 5;
	BitmapFont font;
	
	public LeaderboardMenu(CannonGameInterface game){
		this.game = game;
		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/ARIBLK.TTF"));
        FreeTypeFontParameter param = new FreeTypeFontParameter();
        param.size = 30;
        font = gen.generateFont(param);
        gen.dispose();
	}
	Table table = null;
	int i = 1;
	public void onMenuOpen(){
		if(table != null){
			table.remove();
			i = 1;
		}
		table = new Table();
        table.setFillParent(true);
        this.addActor(table);
        
        TextButtonStyle style = new TextButtonStyle();
        style.font = font;
        
        TextButton button = new TextButton("Top Scores", style);
    	table.add(button);
    	table.row();
    	
        
        ArrayList<GameLog> logs = game.getDatabase().getTopScores(maxNumScores);
        
        for(GameLog log : logs){
        	TextButton button0 = new TextButton(i++ + ". " + String.valueOf(log.getScore()), style);
        	table.add(button0);
        	table.row();
        }
       
        
        TextButton button1 = new TextButton("Return to Main Menu", style);
        table.add(button1);
        table.row();

        button1.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		openMainMenu();
				return true;
			}
        });
	}
	
	private void openMainMenu(){
		this.addAction(Actions.sequence(Actions.fadeOut(game.getHud().getMenuFadeTime()), Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.INIT_MAINMENU);
			}
		})));
	}
}
