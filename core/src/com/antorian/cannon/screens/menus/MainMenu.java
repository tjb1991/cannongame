package com.antorian.cannon.screens.menus;

import com.antorian.cannon.GameState;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class MainMenu extends Stage{
	
	CannonGameInterface game;
	ShapeRenderer shapeRenderer;
	Table table;
	TextButton debugButton;

	public MainMenu(CannonGameInterface game){
		this.game = game;
		table = new Table();
        table.setFillParent(true);
        this.addActor(table);
        
        this.shapeRenderer = new ShapeRenderer();
        
//        TextButtonStyle style = new TextButtonStyle();
//        BitmapFont font = new BitmapFont();
//        font.scale(2);
        
        TextButtonStyle style = new TextButtonStyle();
        // BitmapFont font = new BitmapFont();
         //font.scale(2);
         FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/ARIBLK.TTF"));
         FreeTypeFontParameter param = new FreeTypeFontParameter();
         param.size = 40;
         BitmapFont font = gen.generateFont(param);
         gen.dispose();
         style.font = font;
         
         
        style.font = font;
        
       
        
        TextButton button1 = new TextButton("NEW GAME", style);
        table.add(button1);
        table.row();

        TextButton button2 = new TextButton("LEADERBOARD", style);
        table.add(button2);
        table.row();
        
        TextButton button3 = new TextButton("SETTINGS", style);
        table.add(button3);
        table.row();
                
        
        button1.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		startGame();
				return true;
			}
        });
		
        button2.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		openLeaderboard();
				return true;
			}
        });
        
        button3.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		openAbout();
				return true;
			}
        });
        
       debugButton = new TextButton("Debug", style);
        
        
        debugButton.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		openDebug();
				return true;
			}
        });
        table.add(debugButton);
	}
	
	public Table getTable(){
		return table;
	}
	
	public void setDebugMenuEnabled(boolean bool){
		if(bool){
			debugButton.setDisabled(false);
			debugButton.setText("Debug");
		}
		else{
			debugButton.setDisabled(true);
			debugButton.setText("");
		}
	}
	
	
	private void startGame(){
		this.addAction(Actions.sequence(Actions.fadeOut(game.getHud().getMenuFadeTime()), Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.INIT_COUNTDOWN);
			}
		})));
	}
	
	private void openLeaderboard(){
		this.addAction(Actions.sequence(Actions.fadeOut(game.getHud().getMenuFadeTime()), Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.INIT_LEADERBOARD);
			}
		})));
	}
	
	private void openAbout(){
		this.addAction(Actions.sequence(Actions.fadeOut(game.getHud().getMenuFadeTime()), Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.INIT_SETTINGS);
			}
		})));
	}
	
	private void openDebug(){
		this.addAction(Actions.sequence(Actions.fadeOut(game.getHud().getMenuFadeTime()), Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.INIT_DEBUG);
			}
		})));
	}
	
	
}
