package com.antorian.cannon.screens.menus;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar.ProgressBarStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class LoadingMenu extends Stage{

	TextButton button1;
	ProgressBar bar;
	
	public LoadingMenu(){
		Table table = new Table();
        table.setFillParent(true);
        this.addActor(table);
        
        

        TextButtonStyle style = new TextButtonStyle();
        BitmapFont font = new BitmapFont();
        style.font = font;
        //font.scale(2);
        
        
        Skin skin = new Skin();
        Pixmap pixmap = new Pixmap(45, 45, Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("white", new Texture(pixmap));

        TextureRegionDrawable textureBar = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("data/textures/greenLoadBar.png"))));

        ProgressBarStyle barStyle = new ProgressBarStyle(skin.newDrawable("white", Color.DARK_GRAY), textureBar);
        barStyle.knobBefore = barStyle.knob;

        bar = new ProgressBar(1, 100, 2, false, barStyle);
        bar.setHeight(.05f);
        button1 = new TextButton("Loading...", style);
        table.add(button1);
        table.row();
        table.add(bar);
        table.row();

	}
	
	public void setPercent(float percent){
		button1.setText(String.format("%.1f", percent) + "% Loaded");
		//System.out.println(percent);
		bar.setValue(percent);
	}

}
