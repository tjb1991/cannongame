package com.antorian.cannon.screens.menus;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class BlackMenu extends Stage{

	//Just turn the whole screen black, use it to fade in and out
	public BlackMenu(CannonGameInterface game){
		
		Group background = new Group();
		background.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		this.addActor(background);
		
		Texture blackdot = new Texture(GlobalVariable.textureDir + "blackdot.png");
		blackdot.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		Image image = new Image(blackdot);
		image.setFillParent(true);
		
		background.addActor(image);
	}
	
}
