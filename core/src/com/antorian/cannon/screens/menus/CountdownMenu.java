package com.antorian.cannon.screens.menus;

import com.antorian.cannon.interfaces.CannonGameInterface;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class CountdownMenu extends Stage{
	
	TextButton countdownButton;
	float countdownTime = 0;
	
	public CountdownMenu(CannonGameInterface game){
		
		Table table = new Table();
        table.setFillParent(true);
        this.addActor(table);
        

        TextButtonStyle style = new TextButtonStyle();
        BitmapFont font = new BitmapFont();
        //font.scale(2);
        style.font = font;
        
       
        
        countdownButton = new TextButton("", style);
        table.add(countdownButton);
        table.row();
        
	}
	
	public void start(float time){
		countdownTime = time;
	}
	
	@Override
	public void act(float delta){
		if(countdownTime > 0){
			countdownTime-=delta;
			float current = (float) Math.ceil(countdownTime);
			countdownButton.setText(String.format("%.0f", current));
		}
		else{
			countdownButton.setText("GO!");
		}
		
		super.act(delta);
	}
	
	
	
}
