package com.antorian.cannon.screens.menus;

import com.antorian.cannon.GameState;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class SettingsMenu extends Stage{

	CannonGameInterface game;
	
	public SettingsMenu(final CannonGameInterface game){
		this.game = game;
		
		Table table = new Table();
        table.setFillParent(true);
        this.addActor(table);
        

        TextButtonStyle style = new TextButtonStyle();
//        BitmapFont font = new BitmapFont();
//        style.font = font;
        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/ARIBLK.TTF"));
        FreeTypeFontParameter param = new FreeTypeFontParameter();
        param.size = 30;
        BitmapFont font = gen.generateFont(param);
        gen.dispose();
        style.font = font;
       
        TextButton button1 = new TextButton("Theme: default", style);
        table.add(button1);
        table.row();

        button1.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
        });
        
        final TextButton button2 = new TextButton("Debug Menu: " + Settings.debugMenuOption, style);
        table.add(button2);
        table.row();

        button2.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		Settings.debugMenuOption = !Settings.debugMenuOption;
        		button2.setText("Debug Menu: " + Settings.debugMenuOption);
				return true;
			}
        });
               
        TextButton button99 = new TextButton("Return to Main Menu", style);
        table.add(button99);
        table.row();

        button99.addListener(new InputListener(){
        	public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
        		if(!Settings.debugMenuOption){//Return the defaults if debug disabled
        			Settings.setDebugToDefault();
        			game.setupWorld();
        		}
        		openMainMenu();
				return true;
			}
        });
		
	}
	
	private void openMainMenu(){
		this.addAction(Actions.sequence(Actions.fadeOut(game.getHud().getMenuFadeTime()), Actions.run(new Runnable(){
			@Override
			public void run() {
				game.setGameState(GameState.INIT_MAINMENU);
			}
		})));
	}
}
