package com.antorian.cannon.screens;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.screens.menus.LoadingMenu;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LoadingScreen implements Screen{

	Game game;
	AssetManager assetManager;
	LoadingMenu loadingMenu;
	SpriteBatch batch;
	boolean done = false;
	
	public LoadingScreen(Game game, AssetManager assetManager){
		this.game = game;
		this.assetManager = assetManager;
		
		TextureParameter param = new TextureParameter();
		param.minFilter = TextureFilter.Linear;
		param.magFilter = TextureFilter.Linear;
		param.genMipMaps = true;
		
		FileHandle textureHandle;
		FileHandle soundHandle;
		FileHandle musicHandle;
		if (Gdx.app.getType() == ApplicationType.Android || Gdx.app.getType() == ApplicationType.iOS) {
		   textureHandle = Gdx.files.internal(GlobalVariable.textureDir);
		   soundHandle = Gdx.files.internal(GlobalVariable.soundDir);
		   musicHandle = Gdx.files.internal(GlobalVariable.musicDir);
		} else {
		  textureHandle = Gdx.files.internal("./bin/" + GlobalVariable.textureDir);
		  soundHandle = Gdx.files.internal("./bin/" + GlobalVariable.soundDir);
		  musicHandle = Gdx.files.internal("./bin/" + GlobalVariable.musicDir);
		}
		int numFiles = textureHandle.list().length +
				soundHandle.list().length +
				musicHandle.list().length;
		
		for (FileHandle entry: textureHandle.list()) {
			assetManager.load(GlobalVariable.textureDir + entry.name(), Texture.class, param);
		}
		for (FileHandle entry: soundHandle.list()) {
			assetManager.load(GlobalVariable.soundDir + entry.name(), Sound.class);
		}
		for (FileHandle entry: musicHandle.list()) {
			assetManager.load(GlobalVariable.musicDir + entry.name(), Music.class);
		}
		
		batch = new SpriteBatch();
		loadingMenu = new LoadingMenu();
	}
	
	float progress = 0f;
	PlayScreen playScreen = null;
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		if(done){
			game.setScreen(playScreen);
		}
		else if(assetManager.update()){
			playScreen = new PlayScreen(assetManager);
			done = true;
		}
		
		
		progress = assetManager.getProgress();
//		System.out.println(progress);
		loadingMenu.setPercent(progress * 100);
		
		loadingMenu.act(delta);
		
		batch.begin();
		loadingMenu.draw();
		batch.end();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
	}

}
