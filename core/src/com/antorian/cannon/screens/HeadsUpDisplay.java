package com.antorian.cannon.screens;

import com.antorian.cannon.GameState;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.interfaces.HUD;
import com.antorian.cannon.screens.menus.BlackMenu;
import com.antorian.cannon.screens.menus.CountdownMenu;
import com.antorian.cannon.screens.menus.DeadMenu;
import com.antorian.cannon.screens.menus.DebugMenu;
import com.antorian.cannon.screens.menus.HudMenu;
import com.antorian.cannon.screens.menus.LeaderboardMenu;
import com.antorian.cannon.screens.menus.MainMenu;
import com.antorian.cannon.screens.menus.InGameMenu;
import com.antorian.cannon.screens.menus.SettingsMenu;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;


public class HeadsUpDisplay implements HUD{
	
	public CannonGameInterface game;
	
	private MainMenu mainMenu;
	private SettingsMenu aboutMenu;
	private LeaderboardMenu leaderboardMenu;
	private DebugMenu debugMenu;
	private InGameMenu pauseMenu;
	private BlackMenu blackMenu;
	private DeadMenu deadMenu;
	private CountdownMenu countdownMenu;
	private HudMenu hudMenu;
	
	private float menuFadeTime = 0.25f;
	private float waitTime = 3;
	
	InputMultiplexer hudMultiplexer;
		
	
	public HeadsUpDisplay(CannonGameInterface game){
		this.game = game;
	}
	
	public void show(){
		mainMenu = new MainMenu(game);
		aboutMenu = new SettingsMenu(game);
		leaderboardMenu = new LeaderboardMenu(game);
		debugMenu = new DebugMenu(game);
		pauseMenu = new InGameMenu(game);
		blackMenu = new BlackMenu(game);
		deadMenu = new DeadMenu(game);
		countdownMenu = new CountdownMenu(game);
		hudMenu = new HudMenu(game);
		
		hudMultiplexer = new InputMultiplexer();
		hudMultiplexer.addProcessor(0, hudMenu);
		hudMultiplexer.addProcessor(1, game.getGameStage());		
	}
	
	public void dispose(){
		
	}
	
	public void draw(float delta){
		
		GameState gs = game.getGameState();
		
		switch(gs){
		
		case STARTING:
			//Delay here?
			game.getGameManager().getBall().displayWalkingSprite();
			game.setGameState(GameState.INIT_MAINMENU);
			blackMenu.addAction(Actions.sequence(
					Actions.fadeIn(0),
					Actions.fadeOut(menuFadeTime *4)));
			blackMenu.act(delta);
			blackMenu.draw();
			break;
		
		case INIT_RETURNMAIN_FADE_OUT:
			blackMenu.addAction(Actions.sequence(
					Actions.fadeOut(0),
					Actions.fadeIn(menuFadeTime),
					Actions.run(new Runnable() {
						@Override
						public void run() {
							game.setGameState(GameState.DISP_RETURNMAIN_BLACK_ONCE);
						}
					})
			));
			game.setGameState(GameState.DISP_RETURNMAIN_FADE_OUT);
			break;
			
		case DISP_RETURNMAIN_FADE_OUT:
			blackMenu.act(delta);
			blackMenu.draw();
			break;
			
		case DISP_RETURNMAIN_BLACK_ONCE:
			game.setupWorld();
			game.getGameManager().getBall().displayWalkingSprite();//Display hack of sorts
			blackMenu.draw();
			game.setGameState(GameState.INIT_RETURNMAIN_FADE_IN);
			break;
			
		case INIT_RETURNMAIN_FADE_IN:
			blackMenu.addAction(Actions.sequence(
					Actions.fadeOut(menuFadeTime),
					Actions.run(new Runnable() {
						
						@Override
						public void run() {
							game.setGameState(GameState.INIT_MAINMENU);
						}
					})
			));
			blackMenu.draw();
			game.setGameState(GameState.DISP_RETURNMAIN_FADE_IN);
			break;
			
		case DISP_RETURNMAIN_FADE_IN:
			blackMenu.act(delta);
			blackMenu.draw();
			break;
			
		case INIT_MAINMENU:
			//Set input processor to menu stage
			//Create tween
			if(Settings.menuBoxDisplayDebug){
				mainMenu.getTable().setDebug(true);
			}
			else{
				mainMenu.getTable().setDebug(false);
			}
			if(Settings.debugMenuOption){
				mainMenu.setDebugMenuEnabled(true);
			}
			else{
				mainMenu.setDebugMenuEnabled(false);
			}
			mainMenu.addAction(Actions.fadeIn(0));
			Gdx.input.setInputProcessor(mainMenu);
			game.setGameState(GameState.DISP_MAINMENU);
			blackMenu.act(delta);
			blackMenu.draw();
			break;
			
		case DISP_MAINMENU:
			//Menu will set GameState itself
			blackMenu.act(delta);
			mainMenu.act(delta);
			mainMenu.draw();
			blackMenu.draw();
			break;
		
		case INIT_LEADERBOARD:
			leaderboardMenu.addAction(Actions.fadeIn(menuFadeTime));
			Gdx.input.setInputProcessor(leaderboardMenu);
			leaderboardMenu.onMenuOpen();
			game.setGameState(GameState.DISP_LEADERBOARD);
			break;
			
		case DISP_LEADERBOARD:
			leaderboardMenu.act(delta);
			leaderboardMenu.draw();
			break;
			
		case INIT_SETTINGS:
			aboutMenu.addAction(Actions.fadeIn(menuFadeTime));
			Gdx.input.setInputProcessor(aboutMenu);
			game.setGameState(GameState.DISP_SETTINGS);
			break;
		
		case DISP_SETTINGS:
			aboutMenu.act(delta);
			aboutMenu.draw();
			break;
			
		case INIT_DEBUG:
			debugMenu.addAction(Actions.fadeIn(menuFadeTime));
			Gdx.input.setInputProcessor(debugMenu);
			game.setGameState(GameState.DISP_DEBUG);
			break;
		
		case DISP_DEBUG:
			if(Settings.menuBoxDisplayDebug){
				debugMenu.getTable().setDebug(true);
			}
			else{
				debugMenu.getTable().setDebug(false);
			}
					
			debugMenu.act(delta);
			debugMenu.draw();
			break;
		
		case INIT_COUNTDOWN:
			//setupWorld();			
			countdownMenu.start(waitTime);
			hudMenu.disablePause();
			game.setGameState(GameState.DISP_COUNTDOWN);
			break;
			
		case DISP_COUNTDOWN:
			//world.step(delta, 4, 4);
			
			countdownMenu.act(delta);
			countdownMenu.draw();
			

			break;
		
		case PLAYING_RESUME:
						
			Gdx.input.setInputProcessor(hudMultiplexer);
			game.setGameState(GameState.PLAYING);
			break;
			
		case PLAYING:
			game.getGameManager().getBall().displayFlyingSprite();
			hudMenu.act();
			hudMenu.draw();
			hudMenu.updateScore(game.getGameStage().getScore());//phew that's a stretch, static may have been better
			break;
		
		case INIT_PAUSE:
			Gdx.input.setInputProcessor(pauseMenu);
			pauseMenu.addAction(Actions.fadeIn(0));
			game.setGameState(GameState.DISP_PAUSE);
			break;
			
		case DISP_PAUSE:
			pauseMenu.act(delta);
			pauseMenu.draw();
			break;
		
		case INIT_DEAD:
			deadMenu.addAction(Actions.sequence(Actions.delay(.75f),Actions.fadeIn(menuFadeTime)));//TODO: the .75 is a pause o crap moment
			deadMenu.setFinalScore(game.getGameStage().getScore());
			deadMenu.setFinalCoins(game.getGameStage().getNumCoins());
			Gdx.input.setInputProcessor(deadMenu);
			game.setGameState(GameState.DISP_DEAD);
			break;
			
		case DISP_DEAD:
			deadMenu.act(delta);
			deadMenu.draw();
			break;
			
		case INIT_RESTART_FADE_OUT:
			blackMenu.addAction(Actions.sequence(
					Actions.fadeOut(0),
					Actions.fadeIn(menuFadeTime),
					Actions.run(new Runnable() {
						
						@Override
						public void run() {
							
						}
					}),
					Actions.run(new Runnable() {
						
						@Override
						public void run() {
							
							game.setGameState(GameState.DISP_RESTART_BLACK_ONCE);
						}
					})
			));
			game.setGameState(GameState.DISP_RESTART_FADE_OUT);
			break;
		case DISP_RESTART_FADE_OUT:
			blackMenu.act(delta);
			blackMenu.draw();
			break;
		case DISP_RESTART_BLACK_ONCE:
			game.setupWorld();
			game.getGameManager().getBall().displayWalkingSprite();//Display hack of sorts
			blackMenu.draw();
			game.setGameState(GameState.INIT_RESTART_FADE_IN);
			break;
		case INIT_RESTART_FADE_IN:
			blackMenu.addAction(Actions.sequence(
					Actions.fadeOut(menuFadeTime),
					Actions.run(new Runnable() {
						
						@Override
						public void run() {
							
							game.setGameState(GameState.INIT_COUNTDOWN);
						}
					})
			));
			game.setGameState(GameState.DISP_RESTART_FADE_IN);
			blackMenu.draw();
			break;
		case DISP_RESTART_FADE_IN:
			blackMenu.act(delta);
			blackMenu.draw();
			break;				
		default:
			break;
		
		}
		
	}

	@Override
	public void popup(boolean pauseGame) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public float getMenuFadeTime() {
		return menuFadeTime;
		
	}
	
}
