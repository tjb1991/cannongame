package com.antorian.cannon.screens;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;

import com.antorian.cannon.support.SpriteAccessor;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class AntorianScreen implements Screen{

	Game game;
	TweenManager tweenManager;
	SpriteBatch spriteBatch;
	Texture antorianTexture;
	Sprite splash;
	int logoDisplayTime = 2;
	private AssetManager assetManager;
	
	public AntorianScreen(Game game, AssetManager assetManager){
		this.game = game;
		this.assetManager = assetManager;
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		tweenManager.update(delta);
		
		spriteBatch.begin();
		splash.draw(spriteBatch);
		spriteBatch.end();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		tweenManager = new TweenManager();
		spriteBatch = new SpriteBatch();
		
		Tween.registerAccessor(Sprite.class, new SpriteAccessor());
		
		TextureParameter param = new TextureParameter();
		param.minFilter = TextureFilter.Linear;
		param.magFilter = TextureFilter.Linear;
		param.genMipMaps = true;
		assetManager.load("data/antorianLogoDev.png", Texture.class, param);
		assetManager.finishLoading();
		antorianTexture = assetManager.get("data/antorianLogoDev.png", Texture.class);
		
		splash = new Sprite(antorianTexture);
		splash.setPosition(0, 0);
		
		Tween.set(splash,  SpriteAccessor.ALPHA).target(0).start(tweenManager);
		Tween.to(splash, SpriteAccessor.ALPHA, .4f).target(1).repeatYoyo(1,  1.6f).setCallback(new TweenCallback() {
			
			@Override
			public void onEvent(int type, BaseTween<?> source) {
				game.setScreen(new LoadingScreen(game, assetManager));
			}
		}).start(tweenManager);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
