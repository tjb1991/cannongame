package com.antorian.cannon.screens;


import com.antorian.cannon.CannonGame;
import com.antorian.cannon.GameManager;
import com.antorian.cannon.GameState;
import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.BallInterface;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.interfaces.GameStage;
import com.antorian.cannon.interfaces.HUD;
import com.antorian.cannon.interfaces.SettingsInterface;
import com.antorian.cannon.physics.PlayScreenContactListener;
import com.antorian.cannon.support.database.DatabaseDAO;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.TimeUtils;

public class PlayScreen implements Screen, CannonGameInterface{
		

	private GameState gameState;
	
	private OrthographicCamera camera;
	private OrthographicCamera backgroundCamera;
	private SpriteBatch batch;
	
	private AssetManager assetManager;
	
	private World world;
	private Box2DDebugRenderer debugRenderer;
	
	GameManager gameManager;
	Settings settings;
	HeadsUpDisplay hud;
	DatabaseDAO database;
	
	
	public PlayScreen(AssetManager assetManager){
		this.assetManager = assetManager;
		this.gameState = GameState.STARTING;
		this.settings = new Settings(this);
		
		this.database = new DatabaseDAO(CannonGame.context);
		
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		camera = new OrthographicCamera(1, h/w);
		
		camera.position.y= GlobalVariable.GAME_START_POSITION.y;
		camera.position.x= GlobalVariable.GAME_START_POSITION.x;
		
		backgroundCamera = new OrthographicCamera(1, h/w);
		
		backgroundCamera.position.y= GlobalVariable.GAME_START_POSITION.y;
		backgroundCamera.position.x= GlobalVariable.GAME_START_POSITION.x;
		
		batch = new SpriteBatch();
		world = new World(new Vector2(GlobalVariable.GRAVITY_X, GlobalVariable.GRAVITY_Y), GlobalVariable.DO_SLEEP);
		world.setContactListener(new PlayScreenContactListener());
		gameManager = new GameManager(this, world);
		
		hud = new HeadsUpDisplay(this);
	
	}
	
	public void setupWorld(){
		
		Settings.autoFire = 0;
		
		camera.position.y= GlobalVariable.GAME_START_POSITION.y;
		camera.position.x= GlobalVariable.GAME_START_POSITION.x;
		
		backgroundCamera.position.y= GlobalVariable.GAME_START_POSITION.y;
		backgroundCamera.position.x= GlobalVariable.GAME_START_POSITION.x;
		
		gameManager.gameRestart(world);
		world.step(1/60f, GlobalVariable.VELOCITY_ITERATIONS, GlobalVariable.POSITION_ITERATIONS);
		
		gameManager.update(Gdx.graphics.getDeltaTime(), camera, backgroundCamera);
	}

	private GameState lastState = GameState.PLAYING;//No it's not
	
	float accumulator = 0;
	float timeStep = 1/60f;
	
	private void stepWorld(float delta){
//		float frameTime = Math.min(delta, 0.25f);
//	    accumulator += frameTime;
//	    while (accumulator >= timeStep) {
//	        world.step(timeStep, GlobalVariable.VELOCITY_ITERATIONS, GlobalVariable.POSITION_ITERATIONS);
//	        accumulator -= timeStep;
//	    }
//	    return accumulator/timeStep;

		world.step(delta, GlobalVariable.VELOCITY_ITERATIONS, GlobalVariable.POSITION_ITERATIONS);

	}
	
	@Override
	public void render(float delta) {	

		
	    
	    
	    
		
		if(this.getGameState()!=lastState){
			//System.out.println("Game State: " + this.getGameState());
			this.lastState = this.getGameState();
		}
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.setProjectionMatrix(camera.combined);

		gameManager.updateAnimations(delta, camera, backgroundCamera);
		
		
		switch(gameState){

		case INIT_DEAD:
			
		case DISP_DEAD:
			stepWorld(delta);
			gameManager.updateBallAnimateToBodyOnly();
			break;
		case PLAYING:
			stepWorld(delta);
			gameManager.update(delta, camera, backgroundCamera);
			break;
		default:
			break;
		
		}
		gameManager.render(batch, camera, backgroundCamera);
		
		if(Settings.boxPhysicsDebug){
			debugRenderer.render(world, camera.combined);
		}
		hud.draw(delta);
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {		
		debugRenderer = new Box2DDebugRenderer();
		setupWorld();
		hud.show();
		database.open();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		database.close();
	}

	@Override
	public void pause() {
		if(gameState == GameState.PLAYING){
			gameState = GameState.INIT_PAUSE;
		}
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		world.dispose();
		hud.dispose();
	}

	@Override
	public HUD getHud() {
		return hud;
	}

	@Override
	public GameStage getGameStage() {
		return gameManager;
	}

	@Override
	public AssetManager getAssetManager() {
		return assetManager;
	}

	@Override
	public SettingsInterface getSettings() {
		return settings;
	}

	@Override
	public GameState getGameState() {
		return gameState;
	}

	@Override
	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	@Override
	public GameManager getGameManager() {
		return gameManager;
	}

	@Override
	public OrthographicCamera getCamera() {
		return camera;
	}
	
	@Override
	public OrthographicCamera getBackgroundCamera() {
		return backgroundCamera;
	}
	
	@Override
	public DatabaseDAO getDatabase(){
		return database;
	}
}