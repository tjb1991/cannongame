package com.antorian.cannon.interfaces;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface PowerupActor {
	
	public void update(float delta);
	
	public void render(SpriteBatch batch);
	
	public boolean finished();
	
	public void dispose();
	
}
