package com.antorian.cannon.interfaces;

public interface CannonInterface {

	public float getInitialAngle();
	
	public float getAngle();
	
	public void setAngle(float angle, boolean tween, float tweenSpeed);
	
	public void setSpeed(float speed);
	
	public void trackBall(BallInterface ball);
	
	public void fire();
	
	public void explode();
	
	
}
