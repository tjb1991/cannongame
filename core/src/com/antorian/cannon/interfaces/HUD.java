package com.antorian.cannon.interfaces;

public interface HUD {

	public void popup(boolean pauseGame);
	
	public float getMenuFadeTime();
	
	
}
