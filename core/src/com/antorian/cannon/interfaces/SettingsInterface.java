package com.antorian.cannon.interfaces;

public interface SettingsInterface {
	
	public float getVolume();
	
	public void setVolume(float volume);
	
	public float getSFXVolume();
	
	public void setSFXVolume(float volume);
	
	public void saveSettings();
	
	public void tutorial(boolean on);
	

}
