package com.antorian.cannon.interfaces;

import com.antorian.cannon.GameManager;
import com.antorian.cannon.GameState;
import com.antorian.cannon.support.database.DatabaseDAO;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;

public interface CannonGameInterface {
	
	public HUD getHud();
	
	public GameStage getGameStage();
	
	public AssetManager getAssetManager();
	
	public SettingsInterface getSettings();
	
	public GameState getGameState();
	
	public void setGameState(GameState gameState);
	
	public void setupWorld();
	
	public GameManager getGameManager();
	
	public DatabaseDAO getDatabase();
	
	public OrthographicCamera getCamera();

	public OrthographicCamera getBackgroundCamera();

}
