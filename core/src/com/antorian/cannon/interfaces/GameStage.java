package com.antorian.cannon.interfaces;

import java.util.ArrayList;

import com.badlogic.gdx.InputProcessor;

public interface GameStage extends InputProcessor{
		
	public int getScore();
	
	public void setScore(int n);
	
	public void increaseScoreBy(int i);
	
	
	public int getNumCoins();
	
	public void setNumCoins(int n);
	
	public void increaseNumCoinsBy(int i);
	
	
	public void resetWorld();
	
	
	public ArrayList<CannonInterface> getCannons();
	
	public CannonInterface getCurrentCannon();
	
	public BallInterface getBall();
	
	public void addPowerup(PowerupActor actor);
	
	public void pause();

}
