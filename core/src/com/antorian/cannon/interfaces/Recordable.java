package com.antorian.cannon.interfaces;

public interface Recordable {
	
	public void startRecording();
	
	public void stopRecording();
	
	public void playRecording();
	
	public void playRecordingBackwards();
	
	public void deleteRecording();

	public void playRecordingFromBeginning();
	
	public void playRecordingFromEnd();

}
