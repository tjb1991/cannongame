package com.antorian.cannon.interfaces;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public interface BallInterface {
	
	public boolean isDead();
	
	public Vector2 getPosition();
	
	public void displayWalkingSprite();
	
	public void displayFlyingSprite();
	
	public void setWalkingSprite(Sprite s);
	
	public void setFlyingSprite(Sprite s);
}
