package com.antorian.cannon.physics;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class PlayScreenContactListener implements ContactListener{

	@Override
	public void beginContact(Contact contact) {
		OnCollision a = (OnCollision) contact.getFixtureA().getUserData();
		OnCollision b = (OnCollision) contact.getFixtureB().getUserData();
		if(a != null && b != null){
			a.beginContact(contact, b.getType());
			b.beginContact(contact, a.getType());
		}
	}

	@Override
	public void endContact(Contact contact) {
		OnCollision a = null;
		OnCollision b = null;
		
		if(contact.getFixtureA() != null)
		a = (OnCollision) contact.getFixtureA().getUserData();
		if(contact.getFixtureB() != null)
			b = (OnCollision) contact.getFixtureB().getUserData();
		if(a != null && b != null){
			a.endContact(contact, b.getType());
			b.endContact(contact, a.getType());
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

}
