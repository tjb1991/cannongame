package com.antorian.cannon.physics;

public enum ObjectType {
	cannon,
	ball,
	wall,
	ground, 
	coin,
	cannonside, 
	trampoline, 
	balloon,
	packageorbox, packageorbox_bomb, autofirebox
}
