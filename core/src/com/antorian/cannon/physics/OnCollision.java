package com.antorian.cannon.physics;

import com.badlogic.gdx.physics.box2d.Contact;

public interface OnCollision {
	
	public ObjectType getType();

	public void beginContact(Contact contact, ObjectType objectType);

	public void endContact(Contact contact, ObjectType objectType);

}
