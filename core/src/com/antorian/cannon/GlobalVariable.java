package com.antorian.cannon;

import com.badlogic.gdx.math.Vector2;

public class GlobalVariable {

	//Physics
	public static final float GRAVITY_X = 0f;
	public static final float GRAVITY_Y = -9.8f;
	public static final boolean DO_SLEEP = true; //Improve performance by not simulating Static Bodies
	//Physics world step
	public static final int VELOCITY_ITERATIONS = 6;//for the velocity constraint solver.
	public static final int POSITION_ITERATIONS = 2; //for the position constraint solver. 
	
	//Gameplay
	public static final int NUMBER_OF_CANNONS = 12;
	//Cannons
	public static final float CANNON_INIT_SPEED = 1.2f;
	public static final float CANNON_STEP_SPEED = .4f;
	public static final float CANNON_MAX_SPEED = 2f;
	//Camera
	public static final float CAMERA_INIT_SPEED = .40f;
	public static final float CAMERA_STEP_SPEED = .1f;
	public static final float CAMERA_MAX_SPEED = .55f;
	public static final Vector2 GAME_START_POSITION = new Vector2(-1.25f, -.5f);
	//Character
	public static final float WALKING_SPEED = .45f;
	//Coins
	public static final float CHANCE_OF_COINS = .55f;
	
	//Assets

	public static final String textureDir = "data/textures/";
	public static final String soundDir = "data/sounds/";
	public static final String musicDir = "data/music/";

	/**
	 * Assume that the screen is 1 unit of measurement
	 * If you want a texture to take up half of the screen
	 * without losing the ratio input 
	 * .5, new Vector2(sprite.getWidth, sprite.getHeight)
	 * 
	 * @param amountOfWidth amount of screen width you want taken up
	 * @param originalSize the size of the sprite (width, height)
	 * @return new width, height
	 */
	public static Vector2 getNewSpriteSizeInTermsOfScreenWidth(float amountOfWidth, float width, float height){
		float imageRatioHeight = height/width;
		Vector2 ret = new Vector2(amountOfWidth, amountOfWidth*imageRatioHeight);
		
		return ret;
	}
	
	public static void rotateArray(Object[] array, int index)	{
	    Object[] result;
	    result = new Object[array.length];

	    System.arraycopy(array, index, result, 0, array.length - index);
	    System.arraycopy(array, 0, result, array.length - index, index);
	    System.arraycopy(result, 0, array, 0, array.length);
	}
}
