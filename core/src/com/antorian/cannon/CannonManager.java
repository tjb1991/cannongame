package com.antorian.cannon;

import java.util.ArrayList;
import java.util.Random;

import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.objects.SingleCannon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class CannonManager {
	
	private CannonGameInterface game;
	private World world;
	private PowerupManager powerupManager;
	private Random rand;
	
	private float cannonSpeed = GlobalVariable.CANNON_INIT_SPEED;

	public ArrayList<SingleCannon> cannons;
	
	private int prevCannon;
	private int currentCannon;
	private int topCannon;

	
	public CannonManager(CannonGameInterface game, World world, PowerupManager powerupManager){
		this.game = game;
		this.world = world;
		this.powerupManager = powerupManager;
		this.rand = new Random();
		rand.setSeed(System.currentTimeMillis());
		reinitCannons();
	}
	
	public void reinitCannons(){
		
		cannonSpeed = GlobalVariable.CANNON_INIT_SPEED;
		
		if(cannons!=null){
			for(SingleCannon cannon : cannons){
				if(cannon.body!=null)
					world.destroyBody(cannon.body);
			}
		}
		
		
		cannons = new ArrayList<SingleCannon>();
		
		float futurePos = 0;
		float prevPos = -.5f;
		float side = .5f;
		
		cannons.add(0, new SingleCannon(game, world, new Vector2(side,-.5f), cannonSpeed, 0));
		
		for(int i = 1; i < GlobalVariable.NUMBER_OF_CANNONS; i++){
			futurePos = prevPos+.5f + getRandHeight();
			float angle = (float) Math.atan(futurePos - prevPos);
			
			float shootAngle = (float) (angle + Math.PI);
			if(shootAngle > 2*Math.PI){
				shootAngle -= 2*Math.PI;
			}
			cannons.get(i-1).shootAtAngle = shootAngle;
			
			prevPos = futurePos;
			side = -side;
			
			if(i == 1){
				cannonSpeed = 1.5f;
			}
			else if(i == 2){
				cannonSpeed = 1.8f;
			}
			else
				cannonSpeed = 2f;
			
			cannons.add(i, new SingleCannon(game, world, new Vector2(side ,futurePos), cannonSpeed, angle));
			generatePowerups(i);
		}
		
		topCannon = cannons.size()-1;
		currentCannon = 0;
		prevCannon = topCannon - 1;
		
		//for cheat
		
	}
	
	public void update(float delta){
		for(SingleCannon cannon : cannons){
			cannon.Update(delta);
		}
	}
	
	public void render(SpriteBatch batch){
		for(SingleCannon cannon : cannons){
			cannon.Render(batch);
		}
	}
	
	public void generateNewCannons(OrthographicCamera camera){
		//Camera can see height/2 down so we just loop through the cannons and put the hidden cannons higher
		if(camera.position.y-camera.viewportHeight/2 > cannons.get(currentCannon).getPosition().y+SingleCannon.width*2*SingleCannon.scale){
			Vector2 endPos = cannons.get(topCannon).getPosition();		
			
		    prevCannon = (prevCannon+1) % cannons.size();
			Vector2 previousCannonPos = cannons.get(prevCannon).getPosition();
						
			float futurePos = endPos.y+.5f + getRandHeight();
			float opp = futurePos - previousCannonPos.y;
			float angle = (float) (Math.atan(opp));
			
			float shootAngle = (float) (angle + Math.PI);
			if(shootAngle > 2*Math.PI)
				shootAngle -= 2*Math.PI;
			cannons.get(prevCannon).shootAtAngle = shootAngle;
			//System.out.println("New cannon angle: " + angle + " shoot at angle: " + shootAngle);
						
			cannons.get(currentCannon).resetAngleAndHeight(futurePos, cannonSpeed, angle);	
			//cannons.get(currentCannon).setSpeed(2.0f);
			topCannon = currentCannon;
			
			generatePowerups(currentCannon);
			
			currentCannon = (currentCannon+1) % cannons.size();
			
			
		}
	}
	
	private void generatePowerups(int i){
		
		
		SingleCannon current = cannons.get(i);
		SingleCannon last;
		if(i==0){
			last = cannons.get(cannons.size()-1);
		}
		else
			last = cannons.get(i-1);
		
		Vector2 c1pos = current.getPosition();
		Vector2 c2pos = last.getPosition();
		
		
		powerupManager.generatePowerupAround(current, last);
		
		
	}
	
	private float getRandHeight(){
		return ((float) rand.nextFloat() - .5f)*.5f;
	}
	
	public void touched(){
		for(SingleCannon cannon : cannons){
			cannon.onTouch();
		}
	}
	
	public void pauseFirstCannon(){
		cannons.get(0).body.setActive(false);
	}
	public void resumeFirstCannon(){
		cannons.get(0).body.setActive(true);
	}

}
