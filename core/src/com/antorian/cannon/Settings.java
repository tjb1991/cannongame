package com.antorian.cannon;

import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.interfaces.SettingsInterface;

public class Settings implements SettingsInterface{
	
	public static boolean debugMenuOption = false;

	public static boolean boxPhysicsDebug = false;
	public static boolean cheat = false;
	public static boolean coinsEnabled = true;
	public static boolean menuBoxDisplayDebug = false;
	public static boolean packagesEnabled = true;
	public static boolean trampolineEnabled = true;
	
	public static void setDebugToDefault(){
		Settings.boxPhysicsDebug = false;
		Settings.cheat = false;
		Settings.coinsEnabled = true;
		Settings.menuBoxDisplayDebug = false;
		Settings.packagesEnabled = true;
		Settings.trampolineEnabled = true;
	}
	
	public static int autoFire = 0;
	
	CannonGameInterface game;
	float sFXVolume;
	float volume;
	
	public Settings(CannonGameInterface game){
		this.game = game;
		sFXVolume = .1f;
		volume = 1f;
	}

	@Override
	public float getVolume() {
		return volume;
	}

	@Override
	public void setVolume(float volume) {
		this.volume = volume;
	}

	@Override
	public float getSFXVolume() {
		return sFXVolume;
	}

	@Override
	public void setSFXVolume(float volume) {
		sFXVolume = volume;
	}

	@Override
	public void saveSettings() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void tutorial(boolean on) {
		// TODO Auto-generated method stub
		
	}

	

}
