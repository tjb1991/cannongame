package com.antorian.cannon;

import java.util.ArrayList;
import java.util.Iterator;

import com.antorian.cannon.interfaces.BallInterface;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.interfaces.CannonInterface;
import com.antorian.cannon.interfaces.GameStage;
import com.antorian.cannon.interfaces.PowerupActor;
import com.antorian.cannon.objects.Ball;
import com.antorian.cannon.objects.Ground;
import com.antorian.cannon.objects.ParallaxBackground;
import com.antorian.cannon.objects.SingleCannon;
import com.antorian.cannon.objects.Wall;
import com.antorian.cannon.objects.impl.beta.BetaGround;
import com.antorian.cannon.objects.impl.beta.BetaParallaxBackground;
import com.antorian.cannon.objects.impl.city.CityParallaxBackground;
import com.antorian.cannon.support.database.DatabaseDAO;
import com.antorian.cannon.support.database.GameLog;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;

public class GameManager implements GameStage{
	
	World world;
	CannonManager cannonManager;
	PowerupManager powerupManager;
	
	float cameraSpeed = GlobalVariable.CAMERA_INIT_SPEED;
	int tempScore = 0;
	int numCoins = 0;
	int score = 0;
	
	CannonGameInterface game;
	Ball ball;
	Wall leftWall, rightWall;
	ParallaxBackground parallax;
	Ground ground;
	

	ArrayList<Body> bodiesToTransform;
	ArrayList<Vector2> bodiesVectors;


	public GameManager(CannonGameInterface game, World world){
		
		this.game = game;
		this.world = world;
		this.powerupManager = new PowerupManager(game, world);
		this.cannonManager = new CannonManager(game, world, powerupManager);
		
		leftWall = new Wall(world, new Vector2(-.5f, -.5f), new Vector2(.01f, 10000f), null);
		rightWall = new Wall(world, new Vector2(.5f, -.5f), new Vector2(.01f, 10000f), null);
		ground = new BetaGround(game, world, new Vector2(-1, -.6f), new Vector2(Integer.MAX_VALUE, .01f));
		ball = new Ball(game, world, new Vector2(cannonManager.cannons.get(0).getPosition().cpy().add(-SingleCannon.width*2 + -Ball.radius, 0)));//cannons.get(0).getPosition().add(-SingleCannon.width*2 + -Ball.radius, 0)
		
		parallax = new CityParallaxBackground(game, new Vector2(0,0));
		parallax.setSpeed(new Vector2(GlobalVariable.WALKING_SPEED, 0));
		
		bodiesToTransform = new ArrayList<Body>();
		bodiesVectors = new ArrayList<Vector2>();
	}	
	
	public void gameRestart(World world){
		numCoins = 0;
		tempScore = 0;
		cameraSpeed = GlobalVariable.CAMERA_INIT_SPEED;
		numCoins = 0;
		
		
		ball.reset();
		
		powerupManager.destroyAllPowerups();
		
		cannonManager.reinitCannons();
		
		if(ball != null){
			ball.body.setTransform(cannonManager.cannons.get(0).getPosition().cpy().add(-SingleCannon.width*2 + -Ball.radius, 0), 0);
			ball.resetScore();
		}
		parallax.reset();
		ground.reinitSprites();
		
		bodiesToTransform = new ArrayList<Body>();
		bodiesVectors = new ArrayList<Vector2>();
	}
	
	float ballRange = 0.15f;
	float timeRange = 0.1f;
	
	float pad = 0;
	float ballY;
	float camY;
	
	
	public void updateAnimations(float delta, OrthographicCamera camera, OrthographicCamera backgroundCamera){
		parallax.update(delta);
		ground.update(delta);
	}
	
	float parallaxX = 2;
	
	
	public void update(float delta, OrthographicCamera camera, OrthographicCamera backgroundCamera){
		
		GameState gameState = game.getGameState();
		
		//The .001 is for rounding error -> .5999999 is less than .6 but close enough
		if(cameraSpeed + .001 < GlobalVariable.CAMERA_MAX_SPEED && score != tempScore){
			cameraSpeed += GlobalVariable.CAMERA_STEP_SPEED;
			tempScore = score;
		}
		
		ballY = ball.body.getPosition().y;
		
		ballY = ball.getSprite().getY();
		
		camY = camera.position.y;
		
		if(gameState == GameState.PLAYING){
			camera.translate(new Vector2(0, delta*(cameraSpeed + pad)));
			backgroundCamera.translate(new Vector2(0, delta*(cameraSpeed + pad)));
		}

		
		
		if(gameState == GameState.PLAYING|| 
				   gameState == GameState.INIT_PAUSE ||
				   gameState == GameState.DISP_PAUSE || 
				   gameState == GameState.INIT_DEAD || 
				   gameState == GameState.DISP_DEAD ||
				   gameState == GameState.INIT_RESTART_FADE_OUT ||
				   gameState == GameState.DISP_RESTART_FADE_OUT ||
		   		   gameState == GameState.INIT_RETURNMAIN_FADE_OUT ||
		   		   gameState == GameState.DISP_RETURNMAIN_FADE_OUT){
			
		}
		else if( gameState == GameState.DISP_COUNTDOWN){
			camera.translate(new Vector2(GlobalVariable.WALKING_SPEED*delta, 0));
			backgroundCamera.translate(new Vector2(GlobalVariable.WALKING_SPEED*delta, 0));
		}
		else{
			//parallax.setSpeed(new Vector2(GlobalVariable.WALKING_SPEED, 0));
			//background.setSpeed(new Vector2(0.5f, 0));
			backgroundCamera.translate(new Vector2(GlobalVariable.WALKING_SPEED*delta, 0));
			
		}
		
		camera.update();
		backgroundCamera.update();
		
		cannonManager.generateNewCannons(camera);
		ball.update(delta);
		cannonManager.update(delta);
		 
		pad = (ballY - camY)/(ballRange/timeRange);
		//The pad needs to be:
		/**
		 * (ballY - camY)*[timeToTakeTogetThere or ]delta/LengthOfTimeBuff
		 */
		
		if(ballY + (Ball.radius*2 * Ball.scale) < camY - (.5f*Gdx.graphics.getHeight()/Gdx.graphics.getWidth()))
			game.setGameState(GameState.INIT_DEAD);
//		else if(pad>ballRange*(4/2))
//			pad = ballRange*(5/2);
//		else if(pad>ballRange*(3/2))
//			pad = ballRange*(4/2);
//		else if(pad>ballRange*(2/2))
//			pad = ballRange*(3/2);
//		else if(pad>ballRange*(1/2))
//			pad = ballRange*(2/2);
		else if(pad>ballRange)
			pad = ballRange;
		else if(pad>2*ballRange)
			pad = 2*ballRange;
		else if(pad<-(ballRange*(4/2)))
			pad = -(ballRange*(5/2));
		else if(pad<-(ballRange*(3/2)))
			pad = -(ballRange*(4/2));
		else if(pad<-(ballRange*(2/2)))
			pad = -(ballRange*(3/2));
		else if(pad<-(ballRange*(1/2)))
			pad = -(ballRange*(2/2));
		
		
		
		/**Dispose carefully**/
		if(!world.isLocked()){
			powerupManager.disposeOfHitPowerups();
			Iterator<Body> i = bodiesToTransform.iterator();
			Iterator<Vector2> i2 = bodiesVectors.iterator();
			
			while(i.hasNext()){
				Body body = i.next();
				body.setTransform(i2.next(), 0);
				body.setLinearVelocity(0, 0);
				body.setGravityScale(0);
				i.remove();
				i2.remove();
			}
		}
	}
	
	public void updateBallAnimateToBodyOnly(){
		ball.updateSpriteToBodyPos();
	}
	
	public void render(SpriteBatch batch, Camera camera, OrthographicCamera backgroundCamera){
		
		if(game.getGameState() == GameState.DISP_COUNTDOWN){
			if(camera.position.x >= 0){ //AND ball sprite is in cannon
				camera.position.x = 0;
				//Keep moving penguin
				translateBall(Gdx.graphics.getDeltaTime()*GlobalVariable.WALKING_SPEED ,0);
				parallax.setSpeed(new Vector2(0,0));
				if(ballHitCannon()){
					game.setGameState(GameState.PLAYING_RESUME);
					//hudMenu.enablePause();
				}
			}
			else{
				camera.position.add(Gdx.graphics.getDeltaTime() * GlobalVariable.WALKING_SPEED,0,0);
				camera.update();
				backgroundCamera.position.add(Gdx.graphics.getDeltaTime() * GlobalVariable.WALKING_SPEED,0,0);
				backgroundCamera.update();
				moveBall(camera.position.x, camera.position.y-ball.getSprite().getHeight()/2);	
			}
		}
		else if(game.getGameState() != GameState.PLAYING &&
				game.getGameState() != GameState.INIT_DEAD &&
				game.getGameState() != GameState.DISP_DEAD &&
				game.getGameState() != GameState.INIT_PAUSE &&
				game.getGameState() != GameState.DISP_PAUSE &&
				game.getGameState() != GameState.INIT_RESTART_FADE_OUT &&
				game.getGameState() != GameState.DISP_RESTART_FADE_OUT&&
				game.getGameState() != GameState.INIT_RETURNMAIN_FADE_OUT &&
				game.getGameState() != GameState.DISP_RETURNMAIN_FADE_OUT){
			backgroundCamera.position.add(Gdx.graphics.getDeltaTime() * GlobalVariable.WALKING_SPEED,0,0);
			backgroundCamera.update();
			
			parallax.setSpeed(new Vector2(GlobalVariable.WALKING_SPEED, 0));
		}
		
		parallax.render(backgroundCamera);
		batch.setProjectionMatrix(backgroundCamera.combined);
		batch.begin();
		ground.render(batch);
		batch.end();
		
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		
		ball.Render(batch);
		cannonManager.render(batch);
		powerupManager.render(batch);
		
		batch.end();
		
	}
	
	public int getScore(){
		return score;
	}

	public void moveBall(float x, float y) {
		ball.setTempTransform(x, y);
	}
	public Vector2 getBallTransform(){
		return ball.getTempTransform();
	}
	public boolean ballHitCannon(){
		return ball.hitCannon();
	}
	public void translateBall(float x, float y) {
		ball.tempTranslate(x, y);
	}
	
	

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		ball.touched();
		cannonManager.touched();
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ArrayList<CannonInterface> getCannons() {
		return null;
	}

	@Override
	public CannonInterface getCurrentCannon() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BallInterface getBall() {
		return ball;
	}

	@Override
	public void addPowerup(PowerupActor actor) {
	
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resetWorld() {
		// TODO Auto-generated method stub
		
	}
	
	

	@Override
	public void setScore(int n) {
		score = n;
	}

	@Override
	public void increaseScoreBy(int i) {
		score += i;
	}

	@Override
	public int getNumCoins() {
		return numCoins;
	}

	
	@Override
	public void setNumCoins(int n) {
		numCoins = n;
	}

	
	@Override
	public void increaseNumCoinsBy(int i) {
			numCoins += i;
		
	}

	public void setBodyTransform(Body body, Vector2 vector) {
		bodiesToTransform.add(body);
		bodiesVectors.add(vector);
	}

}
