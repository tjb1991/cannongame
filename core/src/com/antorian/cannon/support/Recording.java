package com.antorian.cannon.support;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Recording {
	
	ArrayList<Recording.SingleFrame> record;
	private int pointer;
	
	public Recording(){
		record = new ArrayList<Recording.SingleFrame>();
		pointer = 0;
	}
	
	/**
	 * Returns true if this is the first frame captured
	 * @param sprite
	 * @return
	 */
	public boolean captureFrame(Sprite sprite){
		record.add(new SingleFrame(sprite));
		pointer++;
		
		if(record.size() == 1)
			return true;
		else
			return false;
	}
	
	public void setPointerToEnd(){
		pointer = record.size()-1;
	}
	
	public void setPointerToBeginning(){
		pointer = 0;
	}
	
	public SingleFrame getFrameThenDecrement(){
		if(pointer == 1 || pointer == 0)
			return record.get(0);
		else{
			SingleFrame ret = record.get((pointer-1));
			pointer--;
			return ret;
		}
	}
	
	public SingleFrame getFrameThenIncrement(){
		if(pointer == record.size()-1)
			return record.get(record.size());
		else
			return record.get(pointer++);
	}

	
	public class SingleFrame{
		
		Vector2 size;
		Vector2 origin;
		Vector2 position;
		Vector2 scale;
		Texture region;
		
		public SingleFrame(Sprite sprite){
			size = new Vector2(sprite.getWidth(), sprite.getHeight());
			origin = new Vector2(sprite.getOriginX(), sprite.getOriginY());
			position = new Vector2(sprite.getX(), sprite.getY());
			scale = new Vector2(sprite.getScaleX(), sprite.getScaleY());
			region = sprite.getTexture();
		}
		
		public Sprite setToFrame(Sprite sprite){
			sprite.setSize(size.x, size.y);
			sprite.setOrigin(origin.x, origin.y);
			sprite.setPosition(position.x, position.y);
			sprite.setScale(scale.x, scale.y);
			sprite.setRegion(region);
			return sprite;
		}
	}
}
