package com.antorian.cannon.support.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

  public static final String TABLE_LOCAL_SCORES = "localscores";
  public static final String COLUMN_LOCAL_SCORES_ID = "_id";
  public static final String COLUMN_LOCAL_SCORES_SCORE = "score";

  private static final String DATABASE_NAME = "cannon.db";
  private static final int DATABASE_VERSION = 1;

  // Database creation sql statement
  private static final String DATABASE_CREATE = "create table "
      + TABLE_LOCAL_SCORES + "(" + COLUMN_LOCAL_SCORES_ID
      + " integer primary key autoincrement, " + COLUMN_LOCAL_SCORES_SCORE
      + " int not null);";

  public MySQLiteHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase database) {
    database.execSQL(DATABASE_CREATE);
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    Log.w(MySQLiteHelper.class.getName(),
        "Upgrading database from version " + oldVersion + " to "
            + newVersion + ", which will destroy all old data");
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCAL_SCORES);
    onCreate(db);
  }
  
  public void deleteDatabaseTables(){
	  this.getWritableDatabase().execSQL("DELETE FROM " + TABLE_LOCAL_SCORES + " WHERE 1 = 1");
	  this.storeScore(new GameLog(0));
  }
  
  public void storeScore(GameLog score){
	SQLiteDatabase db = this.getWritableDatabase();
	  
	ContentValues values = new ContentValues();
	values.put(COLUMN_LOCAL_SCORES_SCORE, score.getScore());
	 
	// insert row
	int _id = (int) db.insert(TABLE_LOCAL_SCORES, null, values);
	 
	score.setId(_id);

  }
  
  public int getTopScore(){
	  SQLiteDatabase db = this.getReadableDatabase();
	  
	    String selectQuery = "SELECT  * FROM " + TABLE_LOCAL_SCORES +
	    		" ORDER BY " + COLUMN_LOCAL_SCORES_SCORE + " DESC";
	 
	 
	    Cursor c = db.rawQuery(selectQuery, null);

	    GameLog log = new GameLog();
	    if (c != null){
	    c.moveToFirst();
	 
	    if(!c.isAfterLast())
	    	log.setScore((c.getInt(c.getColumnIndex(COLUMN_LOCAL_SCORES_SCORE))));
	    }
	    return log.getScore();
  }
  
  public ArrayList<GameLog> getTopScores(int n){
	  SQLiteDatabase db = this.getReadableDatabase();
	  ArrayList<GameLog> logs = new ArrayList<GameLog>();
	    String selectQuery = "SELECT  * FROM " + TABLE_LOCAL_SCORES +
	    		" WHERE " + COLUMN_LOCAL_SCORES_SCORE + " > 0 ORDER BY " + COLUMN_LOCAL_SCORES_SCORE + " DESC LIMIT " + n;
	 
	 
	    Cursor c = db.rawQuery(selectQuery, null);
	 
	    if (c != null){
	        c.moveToFirst();
	        if(!c.isAfterLast())//fugly
		    do{
		    	GameLog log = new GameLog();
			    log.setId(c.getInt(c.getColumnIndex(COLUMN_LOCAL_SCORES_ID)));
			    log.setScore((c.getInt(c.getColumnIndex(COLUMN_LOCAL_SCORES_SCORE))));
			    logs.add(log);
			    c.moveToNext();
		    }while(!c.isAfterLast());
	    }
	    
	 
	    return logs;
  }
 
} 