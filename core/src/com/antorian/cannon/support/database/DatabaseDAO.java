package com.antorian.cannon.support.database;

import java.util.ArrayList;

import android.content.Context;
import android.database.SQLException;

public class DatabaseDAO {
	MySQLiteHelper sqlHelper;
	Context context;
	boolean initCorrectly = false;

  public DatabaseDAO(Context context) {
	  if(context!=null){
		  this.context = context;
		  this.initCorrectly = true;
	  }
  }
  
  private boolean notInitialized(){
	  return !initCorrectly;
  }
  
  public void deleteDatabase(){
	  if(notInitialized())
		  return;
	  sqlHelper.deleteDatabaseTables();
  }

  public boolean open(){
	  if(notInitialized())
		  return false;
	  
	  try{
	  sqlHelper = new MySQLiteHelper(context);
	  }
	  catch(SQLException e){
		  return false;
	  }
	  return true;
  }

  public void close() {
	  if(notInitialized())
		  return;
	  
	  sqlHelper.close();
  }
  
  
  public boolean storeScore(GameLog gameLog){
	  if(notInitialized())
		  return false;
	  
	  sqlHelper.storeScore(gameLog);
	  return true;
  }
  
  public int getTopScore(){
	  if(notInitialized())
		  return -1;
	  
	  return sqlHelper.getTopScore();
  }
  
  public ArrayList<GameLog> getTopScores(int n){
	  ArrayList<GameLog> logs = new ArrayList<GameLog>();
	  if(notInitialized())
		  return logs;
	  logs = sqlHelper.getTopScores(n);
	  return logs;
  }

} 

