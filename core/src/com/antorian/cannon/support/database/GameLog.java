package com.antorian.cannon.support.database;

public class GameLog {
	
	private int id = 0;
	private int score = 0;
	private int coins = 0;
	private int boxesHit = 0;
	
	public GameLog(int score){
		this.setScore(score);
	}
	
	public GameLog(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public int getCoins() {
		return coins;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}

	public int getBoxesHit() {
		return boxesHit;
	}

	public void setBoxesHit(int boxesHit) {
		this.boxesHit = boxesHit;
	}
	 
} 