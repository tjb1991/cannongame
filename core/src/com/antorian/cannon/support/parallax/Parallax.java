package com.antorian.cannon.support.parallax;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Parallax{
    
    private ParallaxLayer[] layers;
   // private Camera camera;
    private SpriteBatch batch;
    private Vector2 speed = new Vector2();
    private Vector2 startPos;
    
    /**
     * @param layers  The  background layers
     * @param speed A Vector2 attribute to point out the x and y speed
     * @param startPos where does the thing start?
     */
    public Parallax(ParallaxLayer[] layers,Vector2 speed, Vector2 startPos){
       this.layers = layers;
       this.speed.set(speed);
       this.startPos = startPos;
       
       this.batch = new SpriteBatch();
    }
    
    public float delta = 0;
    public void update(float delta){
    	this.delta = delta;
    }
    
    private float totalX = 0;  
    public void render(Camera camera){
    	totalX+=speed.x * delta;
    	Vector3 oldPos = camera.position.cpy();
    	//this is why the background won't move! You'll need an animation at this point TODO
    	camera.position.set(totalX, camera.position.y + startPos.y, camera.position.z);
    	
       for(ParallaxLayer layer:layers){
          batch.setProjectionMatrix(camera.projection);
          batch.begin();
          float currentX = - camera.position.x*layer.parallaxRatio.x % ( layer.sprite.getWidth() + layer.padding.x) ;
          
          if( speed.x < 0 )currentX += -( layer.sprite.getWidth() + layer.padding.x);
          do{
             float currentY = - camera.position.y*layer.parallaxRatio.y % ( layer.sprite.getHeight() + layer.padding.y) ;
             if( speed.y < 0 )currentY += - (layer.sprite.getHeight()+layer.padding.y);
             do{
                layer.sprite.setPosition(-camera.viewportWidth/2+currentX + layer.startPosition.x,
                		-camera.viewportHeight/2 + currentY +layer.startPosition.y);
                layer.sprite.draw(batch);
                
                currentY += ( layer.sprite.getHeight() + layer.padding.y );
             }while( currentY < camera.viewportHeight);
             currentX += ( layer.sprite.getWidth()+ layer.padding.x);
          }while( currentX < camera.viewportWidth);
          batch.end();
       }
       
       camera.position.set(oldPos);
    }
    
    public void setSpeed(Vector2 speed){
    	this.speed.set(speed);
    }

    public void reset(){
    	
    }
 }