package com.antorian.cannon.objects;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.BallInterface;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.interfaces.CannonInterface;
import com.antorian.cannon.physics.ObjectType;
import com.antorian.cannon.physics.OnCollision;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

public class SingleCannon implements OnCollision, CannonInterface{
	
	CannonGameInterface game;
	
	private float speed;
	private boolean inMotion = false;
	private boolean hasBeenHit = false;
	private boolean hasBeenFired = false;
	
	public Body body;
	
	private Sprite sprite;
	private Fixture physicsFixture;
	public static float width = .1f;
	public static float height = .05f;
	public static float scale = 2;
	private float density = 0;
	private Texture pipeTexture;
	private TextureRegion pipeRegion;

	//Used for debug cheat
	public float shootAtAngle = 0;
	public boolean goodBoxToHit = false;
	
	@SuppressWarnings("unused")
	private SingleCannon(){}
	
	public SingleCannon(CannonGameInterface game, World world, Vector2 position, float speed, float angle){
		
		this.game = game;
		
		if(position.x < 0){ //Is on left side?
			this.speed = speed;
			angle = -angle;
		}
		else{
			this.speed = -speed;
		}
		
		BodyDef def = new BodyDef();
		def.type = BodyType.KinematicBody;
		def.position.x = position.x;
		def.position.y = position.y;
		def.angle = angle;
		
		body = world.createBody(def);
		
		
		PolygonShape poly = new PolygonShape();	
		
		PolygonShape side1 = new PolygonShape();
		PolygonShape side2 = new PolygonShape();
		
		if(position.x < 0){
			poly.setAsBox(width, height, new Vector2(width , 0), 0);
			side1.setAsBox(width-.01f, .001f, new Vector2(width, .08f), 0);
			side2.setAsBox(width-.01f, .001f, new Vector2(width, -.08f), 0);
		}
		else{
			poly.setAsBox(width, height, new Vector2(-width , 0), 0);
			side1.setAsBox(width-.01f, .001f, new Vector2(-width, .08f), 0);
			side2.setAsBox(width-.01f, .001f, new Vector2(-width, -.08f), 0);
		}
		
		physicsFixture = body.createFixture(poly, density);
		poly.dispose();
		physicsFixture.setUserData(this);
		//physicsFixture.setSensor(true);
		
		
		OnCollision cannonSide = new OnCollision() {
			
			@Override
			public ObjectType getType() {
				// TODO Auto-generated method stub
				return ObjectType.cannonside;
			}
			
			@Override
			public void endContact(Contact contact, ObjectType objectType) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beginContact(Contact contact, ObjectType objectType) {
				// TODO Auto-generated method stub
				
			}
		};
		
		Fixture s1 = body.createFixture(side1, density);
		s1.setUserData(cannonSide);
		side1.dispose();
		
		Fixture s2 = body.createFixture(side2, density);
		s2.setUserData(cannonSide);
		side2.dispose();
		
		pipeTexture = game.getAssetManager().get(GlobalVariable.textureDir + "pipe.png", Texture.class);
		pipeRegion = new TextureRegion(pipeTexture, 0, 0, pipeTexture.getWidth(), pipeTexture.getHeight());
		
		sprite = new Sprite(pipeRegion);
		//sprite.setSize(SingleCannon.width*3, SingleCannon.width*2 * sprite.getHeight() / sprite.getWidth());
		sprite.setScale(scale);
		
		sprite.setSize(SingleCannon.width*2, SingleCannon.height*2);
		if(position.x < 0){
			sprite.setOrigin(sprite.getWidth()/6, height);//TODO: HERE IS SCALE change (whatever you add onto the origin you need to take it away in the rendering)
			sprite.flip(true, false);
		}
		else{
			sprite.setOrigin(width*5/3, height);
		}
		Update(0); //Set initial sprite locations
	}
	
	public void Update(float deltaTime){
		sprite.setRotation(MathUtils.radiansToDegrees * physicsFixture.getBody().getAngle());
		
		if(physicsFixture.getBody().getPosition().x < 0)
			sprite.setPosition((physicsFixture.getBody().getPosition().x - sprite.getWidth()/6), (physicsFixture.getBody().getPosition().y - SingleCannon.height));
		else{
			sprite.setPosition(physicsFixture.getBody().getPosition().x - SingleCannon.width*5/3, physicsFixture.getBody().getPosition().y - SingleCannon.height);
		}
		
		if(inMotion){//TODO VERRRYYYY MESSSY, keeps first cannon from moving early
			if((this.physicsFixture.getBody().getPosition().x > 0 && physicsFixture.getBody().getAngle() <= -Math.PI/2f)){
				inMotion = false;
				physicsFixture.getBody().setAngularVelocity(0);
			}
			else if(physicsFixture.getBody().getAngle() >= Math.PI/2f){
				inMotion = false;
				physicsFixture.getBody().setAngularVelocity(0);
			}
		}
		
	}
	
	public void Render(SpriteBatch batch){
				
		sprite.draw(batch);
		//Be a cheater
		if(Settings.cheat || Settings.autoFire > 0){
			cheat();
		}
		
	}
	
	private void cheat(){
		if(this.inMotion  && this.physicsFixture.getBody().getPosition().x > 0 && (body.getAngle() <= (this.shootAtAngle-3)*-1 || (this.goodBoxToHit && body.getAngle() < -0.15f))){
			game.getGameManager().touchDown(0, 0, 0, 0);
			this.inMotion = false;
			this.goodBoxToHit = false;
		}
		else if(this.inMotion  && this.physicsFixture.getBody().getPosition().x < 0 && (body.getAngle() >= (this.shootAtAngle-3) || (this.goodBoxToHit && body.getAngle() > 0.15f))){
			game.getGameManager().touchDown(0, 0, 0, 0);
			this.inMotion = false;
			this.goodBoxToHit = false;
		}
	}
	
	public void onTouch(){
		if(inMotion){
			inMotion = false;
			physicsFixture.getBody().setAngularVelocity(0);
			if(Settings.autoFire > 0 && !this.hasBeenFired){
				if(this.goodBoxToHit){
					this.goodBoxToHit = false;
				}
				else{
					Settings.autoFire--;
					this.hasBeenFired = true;
				}
			}
				//System.out.println("Best angle to shoot at: " + shootAtAngle + " Shot at angle: " + body.getAngle());
		}
	}
	
	public Vector2 getPosition(){
		return physicsFixture.getBody().getPosition();
	}
	
	/*
	 * To be called when moving lower cannons higher
	 */
	public void resetAngleAndHeight(float y, float speed, float angle){
		
		this.hasBeenHit = false;
		this.hasBeenFired = false;
		
		Vector2 pos = physicsFixture.getBody().getPosition();
		if(pos.x>0)
			speed = -speed;
		else
			angle = -angle;
		
		physicsFixture.getBody().setTransform(new Vector2(pos.x, y),  angle);
		this.speed = speed;
	}
	
	@Override
	public void beginContact(Contact contact, ObjectType objectType) {
		if(objectType == ObjectType.ball){
			if(this.hasBeenHit)
				//System.out.println("Cannon double hit");
			this.hasBeenHit = true;
			this.inMotion = true;
			physicsFixture.getBody().setAngularVelocity(speed);
		}
	}

	@Override
	public void endContact(Contact contact, ObjectType objectType) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public ObjectType getType(){
		return ObjectType.cannon;
	}

	@Override
	public float getInitialAngle() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getAngle() {
		return 0;
	}

	@Override
	public void setAngle(float angle, boolean tween, float tweenSpeed) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void trackBall(BallInterface ball) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void fire() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void explode() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSpeed(float speed) {
		this.speed = speed;
	}

}
