package com.antorian.cannon.objects;

import com.antorian.cannon.GameState;
import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.BallInterface;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.interfaces.Recordable;
import com.antorian.cannon.physics.ObjectType;
import com.antorian.cannon.physics.OnCollision;
import com.antorian.cannon.support.Recording;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Ball implements OnCollision, BallInterface, Recordable{
	
	CannonGameInterface game;
	World world;
	
	public Body body;
	public Fixture physicsFixture;
	public static float radius = .05f;
	public static float density = .65f;
	public static float restitution = .5f;
	public static float scale = 1.25f;
	
	private Animation animation;
	private Sprite sprite;
	private Texture ballTexture;
	TextureRegion ballRegion;
	
	private boolean isRecording = false;
	private Recording recording;
	private boolean isPlayingRecording = false;
	private boolean isPlayingRecordingForward = false;
	
	private boolean displayWalkingSprite = true;
	
	
	public Ball(CannonGameInterface game, World world, Vector2 position){
		this.game = game;
		this.world = world;
		
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		def.position.x = position.x;
		def.position.y = position.y;
		
		body = world.createBody(def);
		CircleShape circle = new CircleShape();
		circle.setRadius(radius);
		
		physicsFixture = body.createFixture(circle, density);
		circle.dispose();
		physicsFixture.setUserData(this);
		physicsFixture.setRestitution(restitution);
		
		ballTexture = game.getAssetManager().get(GlobalVariable.textureDir + "penguin.png", Texture.class);
		ballRegion = new TextureRegion(ballTexture, 0, 0, ballTexture.getWidth(), ballTexture.getHeight());
		
		sprite = new Sprite(ballRegion);
		sprite.setSize(radius, radius * sprite.getHeight() / sprite.getWidth());
		sprite.setOrigin(sprite.getWidth()/2, sprite.getHeight()/2);
		sprite.setPosition(-sprite.getWidth()/2, -sprite.getHeight()/2);
		sprite.scale(scale);
		
		Texture movingPenuginTexture = game.getAssetManager().get(GlobalVariable.textureDir + "penguin2.png", Texture.class);
		TextureRegion movingPenguinRegion = new TextureRegion(movingPenuginTexture, 0, 0, movingPenuginTexture.getWidth(), movingPenuginTexture.getHeight());
		TextureRegion[][] movingRegions = movingPenguinRegion.split(128, 128);
		animation = new Animation(.15f, movingRegions[0][0], movingRegions[0][1], movingRegions[0][2], movingRegions[0][3] );

		isRecording = false;
		recording = new Recording();
	}
	
	public void reset(){
		body.setTransform(GlobalVariable.GAME_START_POSITION.x, 0, 0);
		setTempTransform(GlobalVariable.GAME_START_POSITION.x, GlobalVariable.GAME_START_POSITION.y-this.getSprite().getHeight()/2);
		isRecording = false;
		isPlayingRecording = false;
		recording = new Recording();
		this.displayWalkingSprite = true;
	}
	
	float tempx, tempy;
	boolean tempTransform = false;
	
	public Vector2 getTempTransform(){
		return new Vector2(tempx, tempy);
	}
	
	public void tempTranslate(float x, float y){
		sprite.translate(x, y);
	}
	public void setTempTransform(float x, float y){
		tempTransform = true;
		tempx = (x - sprite.getWidth()/2);
		tempy = (y - sprite.getHeight()/2);
		sprite.setPosition(tempx, tempy);
	}
	public void tempTransformOff(){
		tempTransform = false;
	}
	float time = 0;
	Vector2 actualPosition = new Vector2();

	public void update(float deltaTime){
		

		if(contacted){
			float x = 0;
			float y = 0;
			float angle = contactedBody.getAngle();
			
			if(contactedBody.getPosition().x < 0){
				x = (float) ((2*SingleCannon.width + radius) * Math.cos(angle));
				y = (float) ((2*SingleCannon.width + radius) * Math.sin(angle));
				physicsFixture.getBody().setTransform(contactedBody.getPosition().add(x, y), 0);
			}
			else{
				x = (float) ((2*SingleCannon.width + radius) * Math.cos(angle + Math.PI));
				y = (float) ((2*SingleCannon.width + radius) * Math.sin(angle + Math.PI));
				physicsFixture.getBody().setTransform(contactedBody.getPosition().add(x, y), 0);
			}
		}
		
//		float actualAngle;
//		
//		Vector2 bodyPosition = physicsFixture.getBody().getPosition();
//		bodyPosition.x -= sprite.getWidth()/2;
//		bodyPosition.y -= sprite.getHeight()/2;
//		float bodyAngle = MathUtils.radiansToDegrees * physicsFixture.getBody().getAngle();
//
//        actualPosition.x = bodyPosition.x * alpha + actualPosition.x * (1.0f - alpha);
//        actualPosition.y = bodyPosition.y * alpha + actualPosition.y * (1.0f - alpha);
//        actualAngle = (bodyAngle * alpha + bodyAngle * (1.0f - alpha));
//		
//		
//		float x = actualPosition.x;
//		float y = actualPosition.y;
//		float angle = actualAngle;
		
		
		float x = (physicsFixture.getBody().getPosition().x - sprite.getWidth()/2);
		float y = (physicsFixture.getBody().getPosition().y - sprite.getHeight()/2);
		float angle = MathUtils.radiansToDegrees * physicsFixture.getBody().getAngle();
		
		if(tempTransform){
			x = tempx;
			y = tempy;
		}
		
		if(isRecording){
			sprite.setPosition(x, y);
			sprite.setRotation(angle);
			//System.out.println("We're recording " + sprite.getX() + " " + sprite.getY());
			if(recording.captureFrame(sprite)){
				firstFrameBodyPosition = new Vector2(sprite.getX(), sprite.getY());
			}
		}
		else if(isPlayingRecording && isPlayingRecordingForward){
			//System.out.println("We're playing forward");
			recording.getFrameThenIncrement().setToFrame(sprite);
		}
		else if(isPlayingRecording && !isPlayingRecordingForward){
			//System.out.println("We're playing backward");
			recording.getFrameThenDecrement().setToFrame(sprite);
			if(sprite.getX() == firstFrameBodyPosition.x && sprite.getY() == firstFrameBodyPosition.y){
				//System.out.println("Back to the original frame");
				firstFrameBodyPosition = new Vector2(0,0);
				//System.out.println("Working, now fix that shiz");
				if(body.getTransform().getPosition().x > 0){
					body.applyLinearImpulse(new Vector2(.01f, 0), new Vector2(body.getTransform().getPosition()), true);
					body.setGravityScale(1);
				}
				else{
					body.applyLinearImpulse(new Vector2(-.01f, 0), new Vector2(body.getTransform().getPosition()), true);
					body.setGravityScale(1);
				}
				this.isPlayingRecording = false;
				this.deleteRecording();
			}
		}
		else{
			//System.out.println("Playing Normal");
			sprite.setPosition(x, y);
			sprite.setRotation(angle);
			
			
		}
		
	}
	
	public void updateSpriteToBodyPos(){
		float x = (physicsFixture.getBody().getPosition().x - sprite.getWidth()/2);
		float y = (physicsFixture.getBody().getPosition().y - sprite.getHeight()/2);
		float angle = MathUtils.radiansToDegrees * physicsFixture.getBody().getAngle();
		sprite.setPosition(x, y);
		sprite.setRotation(angle);
	}
	Vector2 firstFrameBodyPosition;
	public void Render(SpriteBatch batch){	
		if(!isPlayingRecording && !isRecording){
			if(!displayWalkingSprite){
				sprite.setRegion(ballRegion);
			}
			else{
				time+=Gdx.graphics.getDeltaTime();
				sprite.setRegion(animation.getKeyFrame(time, true));
			}
		}
		sprite.draw(batch);
		
	}
	
	public Sprite getSprite(){
		return sprite;
	}
	
	//This is only called when you die
	public void resetScore(){
		game.getGameStage().setScore(-1);
		contacted = false;
	}
	
	private boolean contacted = false;
	private Body contactedBody = null;
	private float speedMultiplier = .02f;
	public void touched(){
		
		if(contacted){

			physicsFixture.getBody().setLinearVelocity(0, 0);
			
			float angle = contactedBody.getAngle();
			
			if(contactedBody.getPosition().x > 0){
				angle += Math.PI;
			}
			
			float x = (float) Math.cos(angle);
			float y = (float) Math.sin(angle);
						
			//TODO !!! IMPORTANT: when the ball is released it should take a pole of the angle it's at and check the angle of the next cannon, if it's within the caonnon.threshold let it stick
			
			physicsFixture.getBody().setTransform(physicsFixture.getBody().getPosition(), contactedBody.getAngle());
			this.physicsFixture.getBody().applyLinearImpulse(new Vector2(x * speedMultiplier, y * speedMultiplier), this.physicsFixture.getBody().getPosition(), true);//.applyForceToCenter(new Vector2(x * sppedMultiplier,  y * sppedMultiplier), true);  //.applyForce(new Vector2(x, y), this.physicsFixture.getBody().getPosition(), true);
			contacted = false;
			contactedBody = null;
			
			startRecording();
		}
	}

	@Override
	public void beginContact(Contact contact, ObjectType objectType) {
		//System.out.println("Ball hit a " + objectType.name());
		switch(objectType){
			case cannon:
				//sprite.flip(true, false);When the guy looks like he's flying
				
				if(contacted && isRecording)
					break;
				
				game.getGameStage().increaseScoreBy(1);
				
				contacted = true;
				
				if(contact.getFixtureB() == this.physicsFixture){
					contactedBody = contact.getFixtureA().getBody();
				}
				else
					contactedBody = contact.getFixtureB().getBody();
				if(isRecording){
					stopRecording();
					deleteRecording();
				}
				
				break;
			case ball:
				break;
			case packageorbox_bomb:
			case ground:
				if(game.getGameState() == GameState.PLAYING)
					game.setGameState(GameState.INIT_DEAD);
				break;
			case cannonside:
				break;
			case wall:
				//System.out.println("It hit a cannon side or wall");
				//body.setTransform(firstFrameBodyPosition, 0);//TODO ANGLE
				break;
			case autofirebox:
			case trampoline:
				if(isRecording && !isPlayingRecording){
					body.setLinearVelocity(0,0);
					game.getGameManager().setBodyTransform(body, firstFrameBodyPosition);
					stopRecording();
					playRecordingFromEnd();
				}
			case balloon:
				break;
			case packageorbox:
				break;
			case coin:
				break;
			default:
				break;
		}
        
	}

	@Override
	public void endContact(Contact contact, ObjectType objectType) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public ObjectType getType(){
		return ObjectType.ball;
	}

	public boolean hitCannon() {
		if(sprite.getX()>=body.getTransform().getPosition().x){
			tempTransform = false;
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean isDead() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Vector2 getPosition() {
		return body.getPosition();
	}

	@Override
	public void setWalkingSprite(Sprite s) {
		this.sprite = s;
	}

	@Override
	public void setFlyingSprite(Sprite s) {
		this.sprite = s;
		
	}

	@Override
	public void displayWalkingSprite() {
		this.displayWalkingSprite = true;
		
	}

	@Override
	public void displayFlyingSprite() {
		this.displayWalkingSprite = false;
		
	}
	
	@Override
	public void startRecording() {
		//System.out.println("We started recording");
		isRecording = true;
	}

	@Override
	public void stopRecording() {
		//System.out.println("We're no longer recording");
		isRecording = false;		
	}

	@Override
	public void playRecordingFromBeginning(){
		recording.setPointerToBeginning();
		playRecording();
	}
	
	@Override
	public void playRecording() {
		isPlayingRecording = true;
		isPlayingRecordingForward = true;
	}
	
	@Override
	public void playRecordingFromEnd(){
		recording.setPointerToEnd();
		playRecordingBackwards();
	}

	@Override
	public void playRecordingBackwards() {
		isPlayingRecording = true;
		isPlayingRecordingForward = false;
	}
	
	@Override
	public void deleteRecording(){
		//System.out.println("Deleted Recording");
		recording = new Recording();
	}

}
