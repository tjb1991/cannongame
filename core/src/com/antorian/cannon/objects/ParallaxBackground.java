package com.antorian.cannon.objects;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.support.parallax.Parallax;
import com.antorian.cannon.support.parallax.ParallaxLayer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public abstract class ParallaxBackground {
		Parallax parallax;
		
	public ParallaxBackground(CannonGameInterface game, Vector2 pos){
		//http://www.badlogicgames.com/forum/viewtopic.php?f=17&t=1795
				
		ParallaxLayerConfig[] layerz = getLayers();
		ParallaxLayer[] layers = new ParallaxLayer[layerz.length];

		for(int i = 0; i < layerz.length; i++){
			ParallaxLayerConfig currentLayer = layerz[i];
			Texture texture = game.getAssetManager().get(currentLayer.pathToLayer, Texture.class);
			TextureRegion region = new TextureRegion(texture);
			ParallaxLayer layer = new ParallaxLayer(region, currentLayer.parallaxRatio, currentLayer.startingPosition, currentLayer.padding);
			Vector2 spriteSize = GlobalVariable.getNewSpriteSizeInTermsOfScreenWidth(currentLayer.spriteWidth, region.getRegionWidth(), region.getRegionHeight());
			layer.sprite.setSize(spriteSize.x, spriteSize.y);
			layers[i] = layer;
		}
		parallax = new Parallax(layers, new Vector2(0,0), pos);
	}
	
	public void update(float delta){
		parallax.update(delta);
	}
	
	public void setSpeed(Vector2 vector2){
		parallax.setSpeed(vector2);
	}
	
	public void render(Camera camera){
		parallax.render(camera);
	}
	
	public void reset(){
		parallax.reset();
	}
	
	protected abstract ParallaxLayerConfig[] getLayers();
	
	protected class ParallaxLayerConfig{
		public String pathToLayer;
		public float spriteWidth;
		public Vector2 startingPosition;
		public Vector2 parallaxRatio;
		public Vector2 padding;
		
		public ParallaxLayerConfig(String pathToLayer, float spriteWidth, Vector2 startingPostion, Vector2 parallaxRatio, Vector2 padding){
			this.pathToLayer = pathToLayer;
			this.spriteWidth = spriteWidth;
			this.startingPosition = startingPostion;
			this.parallaxRatio = parallaxRatio;
			this.padding = padding;
		}
	}
}