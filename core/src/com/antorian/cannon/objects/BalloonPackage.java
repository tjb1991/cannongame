package com.antorian.cannon.objects;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.interfaces.PowerupActor;
import com.antorian.cannon.physics.OnCollision;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public abstract class BalloonPackage implements PowerupActor{
	
	public enum BalloonPackageState{
		floating,
		balloonHit,
		balloonPopping,
		balloonPopped,
		packageHit,
		packageAnimating,
		packageGone,
		
		boxFalling, 
		balloonFlyingAway
	};
	
	public static int i;
	protected CannonGameInterface game;
	protected World world;
	
	protected Sound balloonSound;
	protected Sound packageSound;
	
	protected Body body;
	protected Fixture balloonFixture;
	protected Fixture packageFixture;
	protected float density = 1;

	protected Vector2 boxPosition;
	protected Vector2 balloonOffset = new Vector2(0, .25f);
	
	protected float radius = .05f;
	protected Vector2 boxSize = new Vector2(.05f, .05f);
	protected float scale = 1;
	
	protected boolean done = false;
	
	public BalloonPackage(CannonGameInterface game, World world, Vector2 position){
		this.game = game;
		this.world = world;
		this.boxPosition = position;
		balloonSound =  game.getAssetManager().get(getBalloonSoundPath(), Sound.class);
		packageSound =  game.getAssetManager().get(getPackageSoundPath(), Sound.class);
		initializePhysics();
		
	}
	
	protected void initializePhysics(){
		BodyDef def = new BodyDef();
		def.type = BodyType.DynamicBody;
		def.position.x = boxPosition.x;
		def.position.y = boxPosition.y;
		
		body = world.createBody(def);
		body.setGravityScale(0);
		
		//CREATE THE BOX TO BE HELD
		PolygonShape pack = new PolygonShape();
		pack.setAsBox(boxSize.x, boxSize.y, new Vector2(0, 0), 0);
		
		Fixture packageFixture = body.createFixture(pack, density);
		packageFixture.setSensor(true);
		packageFixture.setUserData(getPackageCollider());
		pack.dispose();
				
		//CREATE THE BALLOON TO HOLD THE BOX
		CircleShape circle = new CircleShape();
		circle.setRadius(radius);
		circle.setPosition(balloonOffset);
		
		balloonFixture = body.createFixture(circle, density);
		balloonFixture.setSensor(true);
		circle.dispose();
		balloonFixture.setUserData(this.getBalloonCollider());
	}
	
	@Override
	public void update(float delta){
		if(body.getPosition().y > 
			game.getCamera().position.y + (game.getCamera().viewportHeight * GlobalVariable.NUMBER_OF_CANNONS/2) ||
			body.getPosition().y < 
			game.getCamera().position.y - (game.getCamera().viewportHeight)){
				done = true;
		}
	}
	
	@Override
	public boolean finished() {
		return done;
	}
	
	protected Body getBody(){
		return body;
	}
	
	@Override
	public void dispose(){
		world.destroyBody(this.body);
	}
	
	public abstract String getBalloonSoundPath();
	
	public abstract String getPackageSoundPath();
	
	public abstract OnCollision getBalloonCollider();
	
	public abstract OnCollision getPackageCollider();

}
