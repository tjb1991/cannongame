package com.antorian.cannon.objects;

import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.physics.ObjectType;
import com.antorian.cannon.physics.OnCollision;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public abstract class Ground implements OnCollision{

	//Gat dang frackin repeating textures, screw it I'll do it myself
	protected Sprite[] sprites;
	protected int numGrounds = 10;
	
	protected CannonGameInterface game;
	protected Fixture physicsFixture;
	protected float density = 1f;
	protected Sprite sprite;
	protected boolean display = true;
	
	public Ground(CannonGameInterface game, World world, Vector2 position, Vector2 size){
		this.game = game;
		sprites = new Sprite[numGrounds];
		createPhysics(world, position, size);
		createSprite(position, size);		
	}
	
	Vector2 position;
	Vector2 size;
	
	private void createSprite(Vector2 position, Vector2 size){
		this.position = position;
		this.size = size;
		
		Texture texture = game.getAssetManager().get(getSpriteTexturePath(), Texture.class);
		TextureRegion textureRegion = new TextureRegion(texture);
		sprite = new Sprite(textureRegion);
		sprite.setSize(textureRegion.getRegionWidth()*.002f, textureRegion.getRegionHeight()*.002f);
		sprite.setSize(getSpriteSize().x, getSpriteSize().y);
		
		reinitSprites();
		
	}
	
	public void reinitSprites(){
		current = 0;
		sprite.setPosition(position.x-3f, position.y-sprite.getHeight()+size.y);
		for(int i = 0; i < numGrounds; i++){
			sprites[i] = new Sprite(sprite);
			sprite.setPosition(sprite.getX()+sprite.getWidth(), sprite.getY());
		}
		for(int i = 0; i < numGrounds; i++){
			updateSprites();
		}
		
	}
	
	private void createPhysics(World world, Vector2 position, Vector2 size){
		BodyDef def = new BodyDef();
		def.type = BodyType.StaticBody;
		def.position.x = position.x;
		def.position.y = position.y;
		
		Body box = world.createBody(def);
		
		PolygonShape poly = new PolygonShape();	
		
		poly.setAsBox(size.x, size.y);
		
		physicsFixture = box.createFixture(poly, density);
		poly.dispose();
		box.setBullet(true);
		physicsFixture.setUserData(this);
	}
	int current = 0;
	public void update(float delta){
		
		updateSprites();
		
	}
	
	private void updateSprites(){
		
		OrthographicCamera camera = game.getBackgroundCamera();
		
		if(camera.position.x - (float)camera.viewportWidth/2 > sprites[current].getX()+sprites[current].getWidth()){
			int lastSprite;
			if(current==0)
				lastSprite = numGrounds-1;
			else
				lastSprite = current - 1;
			sprites[current].setPosition(sprites[lastSprite].getX()+sprites[lastSprite].getWidth(), sprite.getY());
			if(current==numGrounds-1)
				current=0;
			else
				current++;
		}
	}
	
	public void render(SpriteBatch batch){
		if(display){

			for(int i = 0; i < numGrounds; i++){
				sprites[i].draw(batch);
			}
		}
	}
	
	protected abstract String getSpriteTexturePath();
	
	protected abstract Vector2 getSpriteSize();
	
	@Override
	public ObjectType getType() {
		return ObjectType.ground;
		
	}

	@Override
	public void beginContact(Contact contact, ObjectType objectType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endContact(Contact contact, ObjectType objectType) {
		// TODO Auto-generated method stub
		
	}
	
}
