package com.antorian.cannon.objects.impl.city;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.objects.ParallaxBackground;
import com.badlogic.gdx.math.Vector2;

public class CityParallaxBackground extends ParallaxBackground{

	public CityParallaxBackground(CannonGameInterface game, Vector2 pos) {
		super(game, pos);
	}

	@Override
	public ParallaxLayerConfig[] getLayers() {
		
		ParallaxLayerConfig[] layers = new ParallaxLayerConfig[3];
		
		String texturePath;
		float textureWidth;
		Vector2 startPosition;
		Vector2 parallaxRatio;
		Vector2 padding;
		
		//Background
		texturePath = GlobalVariable.textureDir + "citySky.png";
		textureWidth = 1f;
		startPosition = new Vector2(0,0);
		parallaxRatio = new Vector2(0.05f, .05f);
		padding = new Vector2(0, 0);
		layers[0] = new ParallaxLayerConfig(texturePath, textureWidth, startPosition, parallaxRatio, padding);
		
		//Mid
		texturePath = GlobalVariable.textureDir + "city.png";
		textureWidth = 1.5f;
		startPosition = new Vector2(0, .5f);
		parallaxRatio = new Vector2(.05f, .025f);
		padding = new Vector2(0, Integer.MAX_VALUE);
		layers[1] = new ParallaxLayerConfig(texturePath, textureWidth, startPosition, parallaxRatio, padding);
		
		
		//stars
		texturePath = GlobalVariable.textureDir + "stars.png";
		textureWidth = 1.5f;
		startPosition = new Vector2(0, 2f);
		parallaxRatio = new Vector2(.05f, .025f);
		padding = new Vector2(0, Integer.MAX_VALUE);
		layers[2] = new ParallaxLayerConfig(texturePath, textureWidth, startPosition, parallaxRatio, padding);
				
		//Mid
//		texturePath = GlobalVariable.textureDir + "city.png";
//		textureWidth = 1.5f;
//		startPosition = new Vector2(0, 2f);
//		parallaxRatio = new Vector2(.05f, .5f);
//		padding = new Vector2(0, 1);
//		layers[2] = new ParallaxLayerConfig(texturePath, textureWidth, startPosition, parallaxRatio, padding);
//				
		
		//Ground
//		texturePath = GlobalVariable.textureDir + "groundFront1.png";
//		textureWidth = 1f;
//		startPosition = new Vector2(0, .1f);
//		parallaxRatio = new Vector2(1f, 1f);
//		padding = new Vector2(0, Integer.MAX_VALUE);
//		layers[2] = new ParallaxLayerConfig(texturePath, textureWidth, startPosition, parallaxRatio, padding);
//				
		return layers;
	}
	
	

}
