package com.antorian.cannon.objects.impl.beta;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.objects.Ground;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class BetaGround extends Ground{

	public BetaGround(CannonGameInterface game, World world, Vector2 position, Vector2 size) {
		super(game, world, position, size);
	}

	@Override
	public String getSpriteTexturePath() {
		return GlobalVariable.textureDir + "groundTile.png";
	}

	@Override
	public Vector2 getSpriteSize() {
		return new Vector2(.6f, .6f);
	}

}
