package com.antorian.cannon.objects.impl.beta;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.objects.ParallaxBackground;
import com.badlogic.gdx.math.Vector2;

public class BetaParallaxBackground extends ParallaxBackground{

	public BetaParallaxBackground(CannonGameInterface game, Vector2 pos) {
		super(game, pos);
	}

	@Override
	public ParallaxLayerConfig[] getLayers() {
		
		ParallaxLayerConfig[] layers = new ParallaxLayerConfig[3];
		
		//Background
		layers[0] = new ParallaxLayerConfig(GlobalVariable.textureDir + "clounds1.png", 1, new Vector2(0, GlobalVariable.GAME_START_POSITION.y), new Vector2(0.05f, .05f),new Vector2(0, 0));
		//Mid
		layers[1] = new ParallaxLayerConfig(GlobalVariable.textureDir + "vegMiddle.png", 1, new Vector2(0, GlobalVariable.GAME_START_POSITION.y), new Vector2(.5f, 1),new Vector2(0, Integer.MAX_VALUE));
		//Ground
		layers[2] = new ParallaxLayerConfig(GlobalVariable.textureDir + "groundFront1.png", 1, new Vector2(0, GlobalVariable.GAME_START_POSITION.y), new Vector2(1f, 1), new Vector2(0, Integer.MAX_VALUE));

				
		return layers;
	}
	
	

}
