package com.antorian.cannon.objects.impl.beta;

import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.objects.impl.GameTheme;

public class BetaTheme extends GameTheme{
		
	@SuppressWarnings("unused")
	private BetaTheme(){
	}
	
	public BetaTheme(CannonGameInterface game) {
		this.ground = null;
		this.parallax = null;
	}

}
