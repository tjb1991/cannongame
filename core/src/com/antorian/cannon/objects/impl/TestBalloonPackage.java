package com.antorian.cannon.objects.impl;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.objects.BalloonPackage;
import com.antorian.cannon.physics.ObjectType;
import com.antorian.cannon.physics.OnCollision;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;

public class TestBalloonPackage extends BalloonPackage{
	
	//You have access to all supertype variables like game, world, and position after initialization
	
	
	private BalloonPackageState balloonState = BalloonPackageState.floating;
	
	private float stateTime;
	private boolean display = true;
	private boolean done = false;
	
	private Animation balloonPopSpriteAnimation;
	TextureRegion balloonRegion;
	private TextureRegion currentFrame;
	private Sprite balloonSprite;
	private Sprite boxSprite;
	
	
	private int FRAME_COL = 7;
	private int FRAME_ROW = 1;
	private float FRAME_DURATION = .0625f;
	private int frameHeight;
	private int frameWidth;
	
	
	public TestBalloonPackage(CannonGameInterface game, World world, Vector2 position) {
		super(game, world, position);
		initializeSprites();
	}
	
	private void initializeSprites(){
		stateTime = 0;
//		frameWidth = coinSheet.getWidth()/FRAME_COL;
//		frameHeight = coinSheet.getHeight()/FRAME_ROW;
//		
//		TextureRegion[][] tmp = TextureRegion.split(coinSheet, frameWidth , frameHeight);
//		TextureRegion[] frames = new TextureRegion[FRAME_COL * FRAME_ROW];
//		int ind = 0;
//		for(int i = 0; i < FRAME_ROW; i++){
//			for(int j = 0; j < FRAME_COL; j++){
//				frames[ind++] = tmp[i][j];
//			}
//		}
//		GlobalVariable.rotateArray(frames, i++%ind);
//		spriteAnimation = new Animation(FRAME_DURATION, frames);
//		
//		currentFrame = spriteAnimation.getKeyFrame(0, true);

		boxSize = new Vector2(.066f, .066f);
		
		Texture boxSheet = game.getAssetManager().get(GlobalVariable.textureDir + "crate.png", Texture.class);
		TextureRegion boxRegion = new TextureRegion(boxSheet);
		boxSprite = new Sprite(boxRegion);		
		boxSprite.setSize(boxSize.x, boxSize.y * boxSprite.getHeight() / boxSprite.getWidth());
		boxSprite.setOrigin(boxSprite.getWidth(), boxSprite.getHeight());
		boxSprite.scale(scale);
		boxSprite.setPosition(boxPosition.x, boxPosition.y);
		
		Texture balloonSheet = game.getAssetManager().get(GlobalVariable.textureDir + "balloonPop.png", Texture.class);
		
		frameWidth = balloonSheet.getWidth()/FRAME_COL;
		frameHeight = balloonSheet.getHeight()/FRAME_ROW;
		
		TextureRegion[][] tmp = TextureRegion.split(balloonSheet, frameWidth , frameHeight);
		TextureRegion[] frames = new TextureRegion[FRAME_COL * FRAME_ROW];
		int ind = 0;
		for(int i = 0; i < FRAME_ROW; i++){
			for(int j = 0; j < FRAME_COL; j++){
				frames[ind++] = tmp[i][j];
			}
		}
		//GlobalVariable.rotateArray(frames, i++%ind);
		balloonPopSpriteAnimation = new Animation(FRAME_DURATION, frames);
		
		currentFrame = balloonPopSpriteAnimation.getKeyFrame(0, false);
		
		balloonSprite = new Sprite(currentFrame);		
		balloonSprite.setSize(radius, radius * balloonSprite.getHeight() / balloonSprite.getWidth());
		balloonSprite.setOrigin(balloonSprite.getWidth(), balloonSprite.getHeight());
		balloonSprite.scale(scale);
		balloonSprite.setPosition(body.getPosition().x + balloonOffset.x, body.getPosition().y + balloonOffset.y);
		
		balloonRegion = balloonPopSpriteAnimation.getKeyFrame(0, false);
		
	}	

	@Override
	public void update(float delta) {
		super.update(delta);
		//currentFrame = spriteAnimation.getKeyFrame(stateTime, true);
		boxSprite.setPosition(body.getPosition().x, body.getPosition().y);
		balloonSprite.setPosition(body.getPosition().x + balloonOffset.x, body.getPosition().y + balloonOffset.y);
		//sprite.setRegion(currentFrame);
		
		switch(balloonState){
		
		case floating:
			balloonSprite.setRegion(balloonRegion);
			break;
		case balloonHit:
			balloonSound.play(game.getSettings().getSFXVolume());
			balloonState = BalloonPackageState.balloonPopping;
			break;
		case balloonPopping:
			stateTime += delta;
			balloonSprite.setRegion(balloonPopSpriteAnimation.getKeyFrame(stateTime, false));
			if(balloonPopSpriteAnimation.isAnimationFinished(stateTime)){
				balloonState = BalloonPackageState.balloonPopped;
				stateTime = 0;
			}
			break;
		case balloonPopped:
			balloonState = BalloonPackageState.boxFalling;
			break;
		case packageHit:
			packageSound.play(game.getSettings().getSFXVolume());
			balloonState = BalloonPackageState.packageAnimating;
			break;
		case packageAnimating:
			balloonState = BalloonPackageState.packageGone;
			break;
		case packageGone:
			balloonState = BalloonPackageState.balloonFlyingAway;
			break;
		case boxFalling:
			break;
		case balloonFlyingAway:
			break;
		default:
			break;
		
		}
	}

	@Override
	public void render(SpriteBatch batch){
		
		if(display){
			if(balloonState == BalloonPackageState.floating ||
					balloonState == BalloonPackageState.balloonHit ||
					balloonState == BalloonPackageState.balloonPopping ||
					balloonState == BalloonPackageState.balloonPopped ||
					balloonState == BalloonPackageState.boxFalling){
				this.boxSprite.draw(batch);
			}
			this.balloonSprite.draw(batch);
		}
		else if(!done){
			this.balloonSound.play(game.getSettings().getSFXVolume());
			this.game.getGameStage().increaseNumCoinsBy(1);
			this.done = true;
		}
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
	}
	

	@Override
	public String getPackageSoundPath() {
		return "data/sounds/cratebreak.mp3";
	}
	
	@Override
	public String getBalloonSoundPath() {
		//https://www.freesound.org/people/muel2002/sounds/266963/
		return "data/sounds/pop.wav";
	}

	@Override
	public OnCollision getBalloonCollider() {
		OnCollision balloonCollider = new OnCollision() {
			
			@Override
			public ObjectType getType() {
				return ObjectType.balloon;
			}
			
			@Override
			public void endContact(Contact contact, ObjectType objectType) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beginContact(Contact contact, ObjectType objectType) {
				
				switch(objectType){
				case ball:		
					if(balloonState == BalloonPackageState.floating){
						body.setGravityScale(0.25f);
						body.setAwake(true);
						//body.setType(BodyType.DynamicBody);
						//display = false;
						//game.getGameStage().safelyDisposeOfBody(this.body);
						balloonState = BalloonPackageState.balloonHit;
					}
					break;
				default:
					break;		
				}
			}
		};
		return balloonCollider;
	}

	@Override
	public OnCollision getPackageCollider() {
		OnCollision packageCollider = new OnCollision() {
			
			@Override
			public ObjectType getType() {
				return ObjectType.packageorbox;
			}
			
			@Override
			public void endContact(Contact contact, ObjectType objectType) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beginContact(Contact contact, ObjectType objectType) {

				switch(objectType){
				case ball:	
					if(balloonState == BalloonPackageState.floating){
						body.setGravityScale(-0.125f);
						body.setAwake(true);
							//display = false;
						balloonState = BalloonPackageState.packageHit;
					}
					break;
				default:
					break;		
				}
				
			}
		};
		
		return packageCollider;
	}

}
