package com.antorian.cannon.objects.impl;

import com.antorian.cannon.objects.Ground;
import com.antorian.cannon.objects.ParallaxBackground;
/**
 * This class is meant to hold the specific parts of a theme for easy switching in settings
 * Ie fade out, switch, fade in
 * @author Tyler
 *
 */
public abstract class GameTheme {
	
	protected ParallaxBackground parallax;
	protected Ground ground;
	
	public GameTheme() {
	}
	
	public ParallaxBackground getParallax(){
		return parallax;
	}
	
	public Ground getGround(){
		return ground;
	}

}
