package com.antorian.cannon.objects.impl;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.Settings;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.objects.BalloonPackage;
import com.antorian.cannon.physics.ObjectType;
import com.antorian.cannon.physics.OnCollision;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.World;

public class TestBalloonPackageAutoFire extends BalloonPackage{
	
	//You have access to all supertype variables like game, world, and position after initialization
	
	
	private BalloonPackageState balloonState = BalloonPackageState.floating;
	
	private float stateTime;
	private boolean display = true;
	
	private Animation balloonPopSpriteAnimation;
	private Animation boxSmashSpriteAnimation;

	private Sprite balloonSprite;
	private Sprite boxSprite;
	
	
	private final int BALLOON_FRAME_COL = 7;
	private final int BALLOON_FRAME_ROW = 1;
	private float BALLOON_FRAME_DURATION = .0625f;
	
	private final int BOX_FRAME_COL = 1;
	private final int BOX_FRAME_ROW = 1;
	private float BOX_FRAME_DURATION = .5f;
	
	private boolean balloonDestroyed = false;
	private boolean boxDestroyed = false;
	
	
	public TestBalloonPackageAutoFire(CannonGameInterface game, World world, Vector2 position) {
		super(game, world, position);
		initializeSprites();
	}
	
	private void initializeSprites(){
		stateTime = 0;

		Texture boxSheet = game.getAssetManager().get(GlobalVariable.textureDir + "autofirebox.png", Texture.class);
		
		int frameWidth = boxSheet.getWidth()/BOX_FRAME_COL;
		int frameHeight = boxSheet.getHeight()/BOX_FRAME_ROW;
		
		TextureRegion[][] tmp = TextureRegion.split(boxSheet, frameWidth , frameHeight);
		TextureRegion[] frames = new TextureRegion[BOX_FRAME_COL * BOX_FRAME_ROW];
		int ind = 0;
		for(int i = 0; i < BOX_FRAME_ROW; i++){
			for(int j = 0; j < BOX_FRAME_COL; j++){
				frames[ind++] = tmp[i][j];
			}
		}
		//GlobalVariable.rotateArray(frames, i++%ind);
		boxSmashSpriteAnimation = new Animation(BOX_FRAME_DURATION, frames);
		
		boxSize = new Vector2(.075f, .075f);
		
		boxSprite = new Sprite(boxSmashSpriteAnimation.getKeyFrame(0, false));		
		boxSprite.setSize(boxSize.x, boxSize.y * boxSprite.getHeight() / boxSprite.getWidth());
		boxSprite.setOrigin(boxSprite.getWidth(), boxSprite.getHeight());
		boxSprite.scale(scale);
		boxSprite.setPosition(boxPosition.x, boxPosition.y);
		
		
		
		Texture balloonSheet = game.getAssetManager().get(GlobalVariable.textureDir + "balloonPop.png", Texture.class);
		
		frameWidth = balloonSheet.getWidth()/BALLOON_FRAME_COL;
		frameHeight = balloonSheet.getHeight()/BALLOON_FRAME_ROW;
		
		tmp = TextureRegion.split(balloonSheet, frameWidth , frameHeight);
		frames = new TextureRegion[BALLOON_FRAME_COL * BALLOON_FRAME_ROW];

		ind = 0;
		for(int i = 0; i < BALLOON_FRAME_ROW; i++){
			for(int j = 0; j < BALLOON_FRAME_COL; j++){
				frames[ind++] = tmp[i][j];
			}
		}
		//GlobalVariable.rotateArray(frames, i++%ind);
		balloonPopSpriteAnimation = new Animation(BALLOON_FRAME_DURATION, frames);
		
		
		balloonSprite = new Sprite(balloonPopSpriteAnimation.getKeyFrame(0, false));		
		balloonSprite.setSize(radius, radius * balloonSprite.getHeight() / balloonSprite.getWidth());
		balloonSprite.setOrigin(balloonSprite.getWidth(), balloonSprite.getHeight());
		balloonSprite.scale(scale);
		balloonSprite.setPosition(body.getPosition().x + balloonOffset.x, body.getPosition().y + balloonOffset.y);
		
		
	}	

	private void updateBoxSpritePos(){
		boxSprite.setPosition(body.getPosition().x, body.getPosition().y);
	}
	
	private void updateBalloonSpritePos(){
		balloonSprite.setPosition(body.getPosition().x + balloonOffset.x, body.getPosition().y + balloonOffset.y);
	}
	
	@Override
	public void update(float delta) {
		super.update(delta);
		
		updateBalloonSpritePos();
		
		switch(balloonState){
		
		case floating:
			balloonSprite.setRegion(balloonPopSpriteAnimation.getKeyFrame(0, false));
			boxSprite.setRegion(boxSmashSpriteAnimation.getKeyFrame(0, false));
			break;
		case balloonHit:
			updateBoxSpritePos();
			balloonSound.play(game.getSettings().getSFXVolume());
			balloonState = BalloonPackageState.balloonPopping;
			break;
		case balloonPopping:
			updateBoxSpritePos();
			stateTime += delta;
			balloonSprite.setRegion(balloonPopSpriteAnimation.getKeyFrame(stateTime, false));
			if(balloonPopSpriteAnimation.isAnimationFinished(stateTime)){
				balloonState = BalloonPackageState.balloonPopped;
				stateTime = 0;
				balloonDestroyed = true;
			}
			break;
		case balloonPopped:
			updateBoxSpritePos();
			balloonState = BalloonPackageState.boxFalling;
			break;
		case packageHit:
			packageSound.play(game.getSettings().getSFXVolume());
			stateTime = BOX_FRAME_DURATION;
			balloonState = BalloonPackageState.packageAnimating;
			break;
		case packageAnimating:
			stateTime += delta;
			boxSprite.setRegion(boxSmashSpriteAnimation.getKeyFrame(stateTime, false));
			if(boxSmashSpriteAnimation.isAnimationFinished(stateTime)){
				balloonState = BalloonPackageState.packageGone;
				stateTime = 0;
				boxDestroyed = true;
			}
			break;
		case packageGone:
			balloonState = BalloonPackageState.balloonFlyingAway;
			break;
		case boxFalling:
			break;
		case balloonFlyingAway:
			break;
		default:
			break;
		
		}
	}
	
	@Override
	public void render(SpriteBatch batch){

		if(display){
			if(!boxDestroyed){
				this.boxSprite.draw(batch);
			}
			if(!balloonDestroyed){
				this.balloonSprite.draw(batch);
			}
		}
		else if(!done){
			this.done = true;
		}
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
	}
	

	@Override
	public String getPackageSoundPath() {
		return "data/sounds/cratebreak.mp3";
	}
	
	@Override
	public String getBalloonSoundPath() {
		//https://www.freesound.org/people/muel2002/sounds/266963/
		return "data/sounds/pop.wav";
	}

	@Override
	public OnCollision getBalloonCollider() {
		OnCollision balloonCollider = new OnCollision() {
			
			@Override
			public ObjectType getType() {
				return ObjectType.balloon;
			}
			
			@Override
			public void endContact(Contact contact, ObjectType objectType) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beginContact(Contact contact, ObjectType objectType) {
				
				switch(objectType){
				case ball:		
					if(balloonState == BalloonPackageState.floating){
						body.setGravityScale(0.25f);
						body.setAwake(true);
						//body.setType(BodyType.DynamicBody);
						//display = false;
						//game.getGameStage().safelyDisposeOfBody(this.body);
						balloonState = BalloonPackageState.balloonHit;
					}
					break;
				default:
					break;		
				}
			}
		};
		return balloonCollider;
	}

	@Override
	public OnCollision getPackageCollider() {
		OnCollision packageCollider = new OnCollision() {
			
			@Override
			public ObjectType getType() {
				return ObjectType.autofirebox;
			}
			
			@Override
			public void endContact(Contact contact, ObjectType objectType) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beginContact(Contact contact, ObjectType objectType) {

				switch(objectType){
				case ball:	
					if(balloonState == BalloonPackageState.floating){
						body.setGravityScale(-0.125f);
						body.setAwake(true);
							//display = false;
						balloonState = BalloonPackageState.packageHit;
						Settings.autoFire = 8;
					}
					break;
				default:
					break;		
				}
				
			}
		};
		
		return packageCollider;
	}

}
