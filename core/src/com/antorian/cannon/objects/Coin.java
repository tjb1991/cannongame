package com.antorian.cannon.objects;

import com.antorian.cannon.GlobalVariable;
import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.interfaces.PowerupActor;
import com.antorian.cannon.physics.ObjectType;
import com.antorian.cannon.physics.OnCollision;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Coin implements OnCollision, PowerupActor{
	public static int i;
	CannonGameInterface game;
	World world;
	
	Sound sound;
	
	Body body;
	Fixture physicsFixture;
	float density = 1;
	
	boolean display = true;
	
	Animation spriteAnimation;
	TextureRegion currentFrame;
	Sprite sprite;
	Vector2 position;
	
	float stateTime;
	
	int FRAME_COL = 10;
	int FRAME_ROW = 1;
	float FRAME_DURATION = .025f;
	
	int frameHeight;
	int frameWidth;
	
	float radius = .05f;
	float scale = 1;
	
	
	public Coin(CannonGameInterface game, World world, Vector2 position){
		this.game = game;
		this.world = world;
		this.position = position;
		sound = game.getAssetManager().get("data/sounds/coin.wav", Sound.class);
		createPhysics();
		createSprite();
	}
	
	private void createPhysics(){
		BodyDef def = new BodyDef();
		def.type = BodyType.KinematicBody;
		def.position.x = position.x;
		def.position.y = position.y;
		
		body = world.createBody(def);
		CircleShape circle = new CircleShape();
		circle.setRadius(radius);
		
		physicsFixture = body.createFixture(circle, density);
		physicsFixture.setSensor(true);
		circle.dispose();
		physicsFixture.setUserData(this);
		
	}
	private void createSprite(){
		
		stateTime = 0;
		Texture coinSheet = game.getAssetManager().get(GlobalVariable.textureDir + "coinsprite.png", Texture.class);
		frameWidth = coinSheet.getWidth()/FRAME_COL;
		frameHeight = coinSheet.getHeight()/FRAME_ROW;
		
		TextureRegion[][] tmp = TextureRegion.split(coinSheet, frameWidth , frameHeight);
		TextureRegion[] frames = new TextureRegion[FRAME_COL * FRAME_ROW];
		int ind = 0;
		for(int i = 0; i < FRAME_ROW; i++){
			for(int j = 0; j < FRAME_COL; j++){
				frames[ind++] = tmp[i][j];
			}
		}
		GlobalVariable.rotateArray(frames, i++%ind);
		spriteAnimation = new Animation(FRAME_DURATION, frames);
		
		currentFrame = spriteAnimation.getKeyFrame(0, true);
		sprite = new Sprite(currentFrame);		
		sprite.setSize(radius, radius * sprite.getHeight() / sprite.getWidth());
		sprite.setOrigin(sprite.getWidth(), sprite.getHeight());
		sprite.scale(scale);
		sprite.setPosition(position.x, position.y);
		
	}
	
	public void update(float delta){
		stateTime += delta;
		currentFrame = spriteAnimation.getKeyFrame(stateTime, true);
		sprite.setRegion(currentFrame);
		
	}
	
	boolean done = false;
	public void render(SpriteBatch batch){
		if(display)
			sprite.draw(batch);
		else if(!done){
			sound.play(game.getSettings().getSFXVolume());
			game.getGameStage().increaseNumCoinsBy(1);
			done = true;
		}
	}
	
	public void dispose(){
		world.destroyBody(this.body);
	}

	@Override
	public ObjectType getType() {
		return ObjectType.coin;
	}

	@Override
	public void beginContact(Contact contact, ObjectType objectType) {
		
		switch(objectType){
		case ball:				
				display = false;
				//game.getGameStage().safelyDisposeOfBody(this.body);
			break;
		default:
			break;		
		}
	}

	@Override
	public void endContact(Contact contact, ObjectType objectType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean finished() {
		return done;
	}
}
