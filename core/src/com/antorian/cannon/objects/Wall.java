package com.antorian.cannon.objects;

import com.antorian.cannon.physics.ObjectType;
import com.antorian.cannon.physics.OnCollision;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Wall implements OnCollision{
	
	private Fixture physicsFixture;
	private float density = 0;
	
	public Wall(World world, Vector2 position, Vector2 size, TextureRegion wallRegion){
		BodyDef def = new BodyDef();
		def.type = BodyType.StaticBody;
		def.position.x = position.x;
		def.position.y = position.y;
		
		Body box = world.createBody(def);
		
		PolygonShape poly = new PolygonShape();	
		
		poly.setAsBox(size.x, size.y);
		
		physicsFixture = box.createFixture(poly, density);
		poly.dispose();
		physicsFixture.setUserData(this);
	}
	
	public void update(float delta){
		
	}
	
	public void render(SpriteBatch batch){
		
	}

	@Override
	public ObjectType getType() {
		return ObjectType.wall;
	}

	@Override
	public void beginContact(Contact contact, ObjectType objectType) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endContact(Contact contact, ObjectType objectType) {
		// TODO Auto-generated method stub
		
	}

}
