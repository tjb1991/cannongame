package com.antorian.cannon;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import com.antorian.cannon.interfaces.CannonGameInterface;
import com.antorian.cannon.interfaces.PowerupActor;
import com.antorian.cannon.objects.BalloonPackage;
import com.antorian.cannon.objects.Coin;
import com.antorian.cannon.objects.SingleCannon;
import com.antorian.cannon.objects.Trampoline;
import com.antorian.cannon.objects.impl.TestBalloonPackage;
import com.antorian.cannon.objects.impl.TestBalloonPackageAutoFire;
import com.antorian.cannon.objects.impl.TestBalloonPackageBomb;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class PowerupManager {
	
	CannonGameInterface game;
	World world;
	
	private Random rand;
	
	private ArrayList<PowerupActor> powerups;
	private ArrayList<PowerupActor> powerupsToDisposeOf;
	
	private float chanceOfCoins = .55f;		//55%
	private float chanceOfBalloon = .75f;	//10%
	
	public PowerupManager(CannonGameInterface game, World world) {
		this.game = game;
		this.world = world;
		
		rand = new Random();
		rand.setSeed(System.currentTimeMillis());
		
		powerups = new ArrayList<PowerupActor>();
		powerupsToDisposeOf = new ArrayList<PowerupActor>();
	}
	
	public void generatePowerupAround(SingleCannon cannon1, SingleCannon cannon2){
		
		Vector2 c1pos = cannon1.getPosition();
		Vector2 c2pos = cannon2.getPosition();
		
		float randomValue = rand.nextFloat();
		if(randomValue < chanceOfCoins){
			generateCoins(c1pos, c2pos);
			generateTrampoline(c1pos);
			generateBallonPackage(cannon1);
		}
		
	}
	
	public void render(SpriteBatch batch){
		Iterator<PowerupActor> i = powerups.iterator();
		while(i.hasNext()){
			PowerupActor actor = i.next();
			
			if(actor.finished()){				
				powerupsToDisposeOf.add(actor);
				i.remove();
			}
			else{
				actor.update(Gdx.graphics.getDeltaTime());
				actor.render(batch);
			}
		}
	}
	
	public void disposeOfHitPowerups(){
		Iterator<PowerupActor> i = powerupsToDisposeOf.iterator();
		while(i.hasNext()){
			PowerupActor actor = i.next();
			actor.dispose();
			i.remove();
		}
	}
	
	public void destroyAllPowerups(){
		Iterator<PowerupActor> i = powerups.iterator();
		while(i.hasNext()){
			PowerupActor actor = i.next();
			actor.dispose();
			i.remove();
		}
	}
	
	private void generateTrampoline(Vector2 cPos){
		if(!Settings.trampolineEnabled)
			return;
		
		Vector2 trampolinePos = cPos.cpy();
		trampolinePos.x = trampolinePos.x*-1;
		powerups.add(new Trampoline(game, world, trampolinePos));

	}
	
	private float packageOffset = 0.125f;
	
	private void generateBallonPackage(SingleCannon cannon1){
		Vector2 c1pos = cannon1.getPosition();
		
		if(!Settings.packagesEnabled){
			return;
		}
		
		Vector2 balloonPos = c1pos.cpy();
		
		balloonPos.x = balloonPos.x*-1;
		
		if(balloonPos.x < 0)
			balloonPos.add(packageOffset, 0);
		else
			balloonPos.sub(packageOffset, 0);
		
		BalloonPackage bPack;
		float rando = rand.nextFloat();
		if(rando <.33){
			bPack = new TestBalloonPackageBomb(game, world, balloonPos);
		}
		else if(rando <.66){
			bPack = new TestBalloonPackageAutoFire(game, world, balloonPos);
		}
		else{
			bPack = new TestBalloonPackage(game, world, balloonPos);
			cannon1.goodBoxToHit = true;
		}
		
		powerups.add(bPack);
	}
	
	private void generateCoins(Vector2 c1pos, Vector2 c2pos){
		if(!Settings.coinsEnabled)
			return;
		
		Vector2 mid = new Vector2((c1pos.x + c2pos.x)/2,(c1pos.y + c2pos.y)/2);
		powerups.add(new Coin(game, world, mid));
		
		Vector2 mid1 = new Vector2((c1pos.x + mid.x)/2, (c1pos.y + mid.y)/2);
		//powerups.add(new Coin(game, world, mid1));
		
		Vector2 mid2 = new Vector2((c2pos.x + mid.x)/2, (c2pos.y + mid.y)/2);
		//powerups.add(new Coin(game, world, mid2));
		
		Vector2 mid3 = new Vector2((mid1.x + mid.x)/2, (mid1.y + mid.y)/2);
		powerups.add(new Coin(game, world, mid3));
		
		Vector2 mid4 = new Vector2((mid2.x + mid.x)/2, (mid2.y + mid.y)/2);
		powerups.add(new Coin(game, world, mid4));
	}

}
