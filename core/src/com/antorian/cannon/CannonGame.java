package com.antorian.cannon;

import com.antorian.cannon.screens.AntorianScreen;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;

import android.content.Context;


public class CannonGame extends Game implements ApplicationListener {
	
	AssetManager assetManager;
	public static Context context = null;
	
	public CannonGame(Context androidContext){
		super();
		CannonGame.context = androidContext;
	}
	
	public CannonGame(){
		super();
	}
	
	@Override
	public void create() {
		assetManager = new AssetManager();
		this.setScreen(new AntorianScreen(this, assetManager));
	}

	@Override
	public void dispose() {
		this.getScreen().dispose();
		assetManager.dispose();
	}

	@Override
	public void render() {		
		this.getScreen().render(Gdx.graphics.getDeltaTime());
	}

	@Override
	public void resize(int width, int height) {
		this.getScreen().resize(width, height);
	}

	@Override
	public void pause() {
		this.getScreen().pause();
	}

	@Override
	public void resume() {
		this.getScreen().resume();
	}
}


/**
Game Call hierarchy

Start the Game:
new CannonGame()
	CannonGame.create() -> Sets the screen to new AntorianScreen()
	CannonGame.render() -> iterated over many times, render current screen
	
	AntorianScreen.render() -> Shows the Antorian Logo sets to LoadingScreen
	LoadingScreen.render() -> Loads all needed textures and sounds
	new PlayScreen() -> S
	
	
	
Displaying the Parallax

When the game state is not playing
	increase parallax 
When the game state is init playing
	init the cannons
	init the ball
	
	
The BACKGROUND CAN'T BE THE FLOORRRRR



Changes for parallax
In parallax removed camera moving back and forth

*/